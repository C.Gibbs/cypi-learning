import tkinter as tk
from tkinter import *
from tkinter import Entry, Label, Button, messagebox, filedialog

from cypi_learning import *

from PIL import ImageTk, Image

import mysql.connector

def logo_large(self, parent, root):                 # Func to set a large logo in the top centre of a frame
    scrWidth = self.winfo_screenwidth()
    ctr = (scrWidth/2)-150

    logo = Image.open("CyPi-LearningLogo.ppm")
    logo = logo.resize((300, 300), Image.ANTIALIAS)
    logo = ImageTk.PhotoImage(logo)
    logoLabel = Label(self, image=logo)
    logoLabel.image = logo
    logoLabel.place(x=ctr, y=100)

def logo_small(self, parent, root):                 # Func to set a small logo in top left corner of a frame
    logo = Image.open("CyPi-LearningLogo.ppm")
    logo = logo.resize((208, 208), Image.ANTIALIAS)
    logo = ImageTk.PhotoImage(logo)
    logoLabel = Label(self, image=logo)
    logoLabel.image = logo
    logoLabel.place(x=25, y=25)

def login_page(self, parent, root):                 # Func containing non-command widgets for login page
    scrWidth = self.winfo_screenwidth()
    ctr = (scrWidth/2)-150

    uNameLabel = Label(self, text="Username:")
    uNameLabel.configure(bg="#CFF6FF")
    uNameLabel.place(x=ctr, y=425)

    pWordLabel = Label(self, text="Password:")
    pWordLabel.configure(bg="#CFF6FF")
    pWordLabel.place(x=ctr, y=500)

def create_student_page(self, parent, root):        # Func containing non-command widgets for create student page
    scrWidth = self.winfo_screenwidth()
    scrHeight = self.winfo_screenheight()
    ctr = (scrWidth/2)-150

    uNameLabel = Label(self, text="Create Username:")
    uNameLabel.configure(bg="#CFF6FF")
    uNameLabel.place(x=ctr, y=425)

    pWordLabel = Label(self, text="Create Password:")
    pWordLabel.configure(bg="#CFF6FF")
    pWordLabel.place(x=ctr, y=500)

def student_help_page(self, parent, root):          # Func containing non-command widgets for student help page
    scrWidth = self.winfo_screenwidth()
    ctr = (scrWidth/2)-150

    label1 = Label(self, text="Welcome to the CyPi-Learning application. \n As a student user, this application will allow you to explore the different topics from within the CyBOK in a controlled environment. \n Some of these activities will be self-contained within the application, whilst others will require you to make use of the local Raspbian operating system. \n The hope is that you will enjoy yourself whilst learning new information. \n \n Upon logging into the system, you are placed onto the landing page, which will give you nineteen different topic buttons to choose from. \n Select one of these to view the available activities. If you do not like the look of any, the back button in the bottom right will take you back to the landing page. \n To run an activity, simply click its button. Each activity will open with instructions on what to do. \n \n There is also a button for account options. \n This will allow you to change your login password. \n \n Enjoy!", font=80)
    label1.configure(bg="#CFF6FF")
    label1.place(x=ctr-475, y=300)

def student_acc_op_page(self, parent, root):        # Func containing non-command widgets for student account page
    scrWidth = self.winfo_screenwidth()
    ctr = (scrWidth/2)-150

    pWordLabel = Label(self, text="Update Your Password:")
    pWordLabel.configure(bg="#CFF6FF")
    pWordLabel.place(x=ctr, y=425)

    pWordLabelUpdate2 = Label(self, text="Retype Your New Password:")
    pWordLabelUpdate2.configure(bg="#CFF6FF")
    pWordLabelUpdate2.place(x=ctr, y=500)

def staff_help_page(self, parent, root):            # Func containing non-command widgets for staff help page
    scrWidth = self.winfo_screenwidth()
    ctr = (scrWidth/2)-150

    label1 = Label(self, text="Welcome to the CyPi-Learning application. \n As an admin user, this application will allow you to explore the different topics from within the CyBOK in a controlled environment. \n Some of these activities will be self-contained within the application, whilst others will require you to make use of the local Raspbian operating system. \n The hope is that you will enjoy yourself whilst learning new information. \n \n Upon logging into the system, you are placed onto the landing page, which will give you nineteen different topic buttons to choose from. \n Select one of these to view the available activities. If you do not like the look of any, the back button in the bottom right will take you back to the landing page. \n To run an activity, simply click its button. Each activity will open with instructions on what to do. \n \n As an admin, you also have the ability to add new activities, delete them and update them. \n For each of these options, you must first enter the intended file name in the filename field. \n To add a file, you then click the add button which will open a file dialog window. Simply navigate to the file you wish to upload and click open. \n To remove a file, simply click the remove button after typing the filename. \n To update a file, enter the filename and follow the file dialog to the file you wish to update that activity with. \n \n An activity template file has been provided on the system, which you may adapt to create new activities. \n \n There is also a button for account options. \n This will allow you to change your login password or create a new admin account. \n \n Enjoy!", font=80)
    label1.configure(bg="#B6F1FF")
    label1.place(x=ctr-475, y=300)

def staff_acc_op_page(self, parent, root):          # Func containing non-command widgets for staff account page
    scrWidth = self.winfo_screenwidth()
    ctr = (scrWidth/2)-150

    pWordLabel = Label(self, text="Update Your Password:")
    pWordLabel.configure(bg="#B6F1FF")
    pWordLabel.place(x=ctr, y=300)

    pWordLabelUpdate2 = Label(self, text="Retype Your New Password:")
    pWordLabelUpdate2.configure(bg="#B6F1FF")
    pWordLabelUpdate2.place(x=ctr, y=375)

    uNameLabel = Label(self, text="Create Username:")
    uNameLabel.configure(bg="#B6F1FF")
    uNameLabel.place(x=ctr, y=525)

    pWordLabel = Label(self, text="Create Password:")
    pWordLabel.configure(bg="#B6F1FF")
    pWordLabel.place(x=ctr, y=600)

def file_label(self, parent, root):                 # Func containing label widget for file name input
    scrWidth = self.winfo_screenwidth()
    scrHeight = self.winfo_screenheight()

    fileNameLabel = Label(self, text="Enter File Name:", bg="#B6F1FF")
    fileNameLabel.place(x=scrWidth-1040, y=scrHeight-180)
