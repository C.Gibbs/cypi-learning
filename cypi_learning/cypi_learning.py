import tkinter as tk
from tkinter import *
from tkinter import Entry, Label, Button, messagebox, filedialog

from PIL import ImageTk, Image

import mysql.connector

from widgets import *

global user

class Application(tk.Tk):                           # Application Class
    def __init__(self):
        super().__init__()

        self.title("CyPi-Learning")
        scrWidth = self.winfo_screenwidth()
        scrHeight = self.winfo_screenheight()
        self.geometry("%dx%d" % (scrWidth, scrHeight))
        self.resizable(width=False, height=False)

        container = tk.Frame(self)
        container.pack(side="top", fill="both", expand=True)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        self.frames = {}
        for F in (loginPage, createStudentPage, studentLandingPage, studentHelpPage, studentAccountOpPage, staffLandingPage, staffHelpPage, staffAccountOpPage, staffRMandG, \
        staffLawandReg, staffHumanFactors, staffPandOR, staffMandAT, staffAdversarialBehaviours, staffSOandIM, staffForensics, staffCryptography, staffOSandVS, staffDSS, \
        staffAAandA, staffSoftwareSecurity, staffWandMS, staffSSL, staffNetworkSecurity, staffHardwareSecurity, staffCyberPhysicalSystems, staffPLandTS, studentRMandG,\
        studentLawandReg, studentHumanFactors, studentPandOR, studentMandAT, studentAdversarialBehaviours, studentSOandIM, studentForensics, studentCryprography, studentOSandVS, \
        studentDSS, studentAAandA, studentSoftwareSecurity, studentWandMS, studentSSL, studentNetworkSecurity, studentHardwareSecurity, studentCyberPhysicalSystems, studentPLandTS):
            frame = F(container, self)
            self.frames[F] = frame
            frame.grid(row=0, column=0, sticky="nsew")

        self.show_frame(loginPage)

    def show_frame(self, cont):                     # Func to set a top frame
        frame = self.frames[cont]
        frame.tkraise()

class loginPage(tk.Frame):                          # Class for staff and student login page
    def __init__(self, parent, root):
        super().__init__(parent)

        global user_name

        def login():                                # Func to check user details against database for login
            if uName.get() == "" or pWord.get() == "":
                tk.messagebox.showerror("Login Error", "Enter details in all fields")
                uName.delete(0, 'end')
                pWord.delete(0, 'end')
            else:
                connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='users')
                cursordb = connectiondb.cursor()
                sql = "SELECT * FROM users WHERE Username = %s AND Password = %s"
                user_check = uName.get()
                pass_check = pWord.get()
                cursordb.execute(sql, [(user_check), (pass_check)])
                check_results = cursordb.fetchall()
                global user_name
                user_name = user_check
                if check_results:
                    for i in check_results:
                        tk.messagebox.showinfo("Login", "Successful Login")
                        uName.delete(0, 'end')
                        pWord.delete(0, 'end')
                        admin_check = "SELECT Username, UserType FROM users WHERE Username = %s"
                        cursordb.execute(admin_check, [(user_check)])
                        uInfo = cursordb.fetchone()
                        if uInfo[1] == 'Admin':
                            root.show_frame(staffLandingPage)
                        elif uInfo[1] == 'Student':
                            root.show_frame(studentLandingPage)
                        break
                else:
                    tk.messagebox.showerror("Login Error", "Not in system")
                    uName.delete(0, 'end')
                    pWord.delete(0, 'end')
                connectiondb.commit()
                connectiondb.close()

        self.configure(bg="#CFF6FF")

        scrWidth = self.winfo_screenwidth()
        ctr = (scrWidth/2)-150

        logo_large(self, parent, root)
        login_page(self, parent, root)

        uName = Entry(self, bg="#FFFFFF", width=37)
        uName.place(x=ctr, y=450)

        pWord = Entry(self, bg="#FFFFFF", width=37, show="*")
        pWord.place(x=ctr, y=525)

        loginBut = Button(self, text="Login", width=15, command=lambda: login())
        loginBut.configure(bg="#83B4B3", activebackground="#097392")
        loginBut.place(x=ctr+75, y=575)

        createAccBut = Button(self, text="Create Account", width=15, command=lambda: root.show_frame(createStudentPage))
        createAccBut.configure(bg="#83B4B3", activebackground="#097392")
        createAccBut.place(x=ctr+75, y=625)

class createStudentPage(tk.Frame):                  # Class for student user creation page
    def __init__(self, parent, root):
        super().__init__(parent)

        def add_user():                             # Func to add user input details to the user database as a student account
            if uName.get() == "" or pWord.get() == "":
                tk.messagebox.showerror("MySQL Connection", "Enter details in all fields")
                uName.delete(0, 'end')
                pWord.delete(0, 'end')
            else:
                try:
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='users')
                    cursordb = connectiondb.cursor()
                    cursordb.execute("INSERT INTO users VALUES(%s,%s,%s)", (uName.get(), pWord.get(), "Student"))
                    connectiondb.commit()
                    connectiondb.close()
                    uName.delete(0, 'end')
                    pWord.delete(0, 'end')
                    tk.messagebox.showinfo("MySQL Connection", "New Student Created")
                except:
                    tk.messagebox.showerror("MySQL Connection", "Error: Username Already In Use")

        self.configure(bg="#CFF6FF")

        scrWidth = self.winfo_screenwidth()
        scrHeight = self.winfo_screenheight()
        ctr = (scrWidth/2)-150

        logo_large(self, parent, root)
        create_student_page(self, parent, root)

        uName = Entry(self, bg="#FFFFFF", width=37)
        uName.place(x=ctr, y=450)

        pWord = Entry(self, bg="#FFFFFF", width=37, show="*")
        pWord.place(x=ctr, y=525)

        loginBut = Button(self, text="Create New Student", width=15, command=add_user)
        loginBut.configure(bg="#83B4B3", activebackground="#097392")
        loginBut.place(x=ctr+75, y=575)

        backBut = Button(self, text="Back", width=15, command=lambda: self.lower())
        backBut.configure(bg="#83B4B3", activebackground="#097392")
        backBut.place(x=scrWidth-185, y=scrHeight-150)

class studentLandingPage(tk.Frame):                 # Class for the student landing page
    def __init__(self, parent, root):
        super().__init__(parent)

        self.configure(bg="#CFF6FF")

        scrWidth = self.winfo_screenwidth()
        scrHeight = self.winfo_screenheight()

        logo_small(self, parent, root)

        logoutBut = Button(self, text="Logout", width=15, command=lambda: root.show_frame(loginPage))
        logoutBut.configure(bg="#83B4B3", activebackground="#097392")
        logoutBut.place(x=scrWidth-185, y=25)

        helpBut = Button(self, text="Help", width=15, command=lambda: root.show_frame(studentHelpPage))
        helpBut.configure(bg="#83B4B3", activebackground="#097392")
        helpBut.place(x=scrWidth-360, y=25)

        studentAccOpBut = Button(self, text="Account Options", width=15)
        studentAccOpBut.configure(bg="#83B4B3", activebackground="#097392", command=lambda: root.show_frame(studentAccountOpPage))
        studentAccOpBut.place(x=scrWidth-535, y=25)

        rmgBut = Button(self, text="Risk Management and Governance", width=23, height=7, wraplength=200, command=lambda: root.show_frame(studentRMandGPage))
        rmgBut.configure(bg="#83B4B3", activebackground="#097392")
        rmgBut.place(x=25, y=275)
        lrBut = Button(self, text="Law and Regulation", width=23, height=7, wraplength=200, command=lambda: root.show_frame(studentLawandReg))
        lrBut.configure(bg="#83B4B3", activebackground="#097392")
        lrBut.place(x=300, y=275)
        hfBut = Button(self, text="Human Factors", width=23, height=7, wraplength=200, command=lambda: root.show_frame(studentHumanFactors))
        hfBut.configure(bg="#83B4B3", activebackground="#097392")
        hfBut.place(x=575, y=275)
        porBut = Button(self, text="Privacy and Online Rights", width=23, height=7, wraplength=200, command=lambda: root.show_frame(studentPandOR))
        porBut.configure(bg="#83B4B3", activebackground="#097392")
        porBut.place(x=850, y=275)
        matBut = Button(self, text="Malware and Attack Technologies", width=23, height=7, wraplength=200, command=lambda: root.show_frame(studentMandAT))
        matBut.configure(bg="#83B4B3", activebackground="#097392")
        matBut.place(x=1125, y=275)
        abBut = Button(self, text="Adversarial Behaviours", width=23, height=7, wraplength=200, command=lambda: root.show_frame(studentAdversarialBehaviours))
        abBut.configure(bg="#83B4B3", activebackground="#097392")
        abBut.place(x=1400, y=275)
        soimBut = Button(self, text="Security Operations and Incident Management", width=23, height=7, wraplength=200, command=lambda: root.show_frame(studentSOandIM))
        soimBut.configure(bg="#83B4B3", activebackground="#097392")
        soimBut.place(x=1675, y=275)
        fBut = Button(self, text="Forensics", width=23, height=7, wraplength=200, command=lambda: root.show_frame(studentForensics))
        fBut.configure(bg="#83B4B3", activebackground="#097392")
        fBut.place(x=1950, y=275)
        cBut = Button(self, text="Cryptography", width=23, height=7, wraplength=200, command=lambda: root.show_frame(studentCryprography))
        cBut.configure(bg="#83B4B3", activebackground="#097392")
        cBut.place(x=25, y=450)
        osvsBut = Button(self, text="Operating Systems and Virtualisation Security", width=23, height=7, wraplength=200, command=lambda: root.show_frame(studentOSandVS))
        osvsBut.configure(bg="#83B4B3", activebackground="#097392")
        osvsBut.place(x=300, y=450)
        dssBut = Button(self, text="Distributed Systems Security", width=23, height=7, wraplength=200, command=lambda: root.show_frame(studentDSS))
        dssBut.configure(bg="#83B4B3", activebackground="#097392")
        dssBut.place(x=575, y=450)
        aaaBut = Button(self, text="Authentication, Authorisation and Accountability", width=23, height=7, wraplength=200, command=lambda: root.show_frame(studentAAandA))
        aaaBut.configure(bg="#83B4B3", activebackground="#097392")
        aaaBut.place(x=850, y=450)
        ssBut = Button(self, text="Software Security", width=23, height=7, wraplength=200, command=lambda: root.show_frame(studentSoftwareSecurity))
        ssBut.configure(bg="#83B4B3", activebackground="#097392")
        ssBut.place(x=1125, y=450)
        wmsBut = Button(self, text="Web and Mobile Security", width=23, height=7, wraplength=200, command=lambda: root.show_frame(studentWandMS))
        wmsBut.configure(bg="#83B4B3", activebackground="#097392")
        wmsBut.place(x=1400, y=450)
        sslBut = Button(self, text="Secure Software Lifecycle", width=23, height=7, wraplength=200, command=lambda: root.show_frame(studentSSL))
        sslBut.configure(bg="#83B4B3", activebackground="#097392")
        sslBut.place(x=1675, y=450)
        nsBut = Button(self, text="Network Security", width=23, height=7, wraplength=200, command=lambda: root.show_frame(studentNetworkSecurity))
        nsBut.configure(bg="#83B4B3", activebackground="#097392")
        nsBut.place(x=1950, y=450)
        hsBut = Button(self, text="Hardware Security", width=23, height=7, wraplength=200, command=lambda: root.show_frame(studentHardwareSecurity))
        hsBut.configure(bg="#83B4B3", activebackground="#097392")
        hsBut.place(x=25, y=625)
        cpsBut = Button(self, text="Cyber Physical Systems", width=23, height=7, wraplength=200, command=lambda: root.show_frame(studentCyberPhysicalSystems))
        cpsBut.configure(bg="#83B4B3", activebackground="#097392")
        cpsBut.place(x=300, y=625)
        pltsBut = Button(self, text="Physical Layer and Telecommunications Security", width=23, height=7, wraplength=200, command=lambda: root.show_frame(studentPLandTS))
        pltsBut.configure(bg="#83B4B3", activebackground="#097392")
        pltsBut.place(x=575, y=625)

class studentHelpPage(tk.Frame):                    # Class for the student help page

    def __init__(self, parent, root):
        super().__init__(parent)

        self.configure(bg="#CFF6FF")

        scrWidth = self.winfo_screenwidth()
        scrHeight = self.winfo_screenheight()

        logo_small(self, parent, root)
        student_help_page(self, parent, root)

        backBut = Button(self, text="Back", width=15, command=lambda: self.lower())
        backBut.configure(bg="#83B4B3", activebackground="#097392")
        backBut.place(x=scrWidth-185, y=scrHeight-150)

class studentAccountOpPage(tk.Frame):               # Class for the student account page
    def __init__(self, parent, root):
        super().__init__(parent)

        global user_name

        def update_student_user():                  # Func to update the student password in the user database
            global user_name
            if pWordUpdate.get() == "" or pWordUpdate2.get() == "":
                tk.messagebox.showerror("Update Error", "Please enter details in both fields")
                pWordUpdate.delete(0, 'end')
                pWordUpdate2.delete(0, 'end')
            elif pWordUpdate.get() != pWordUpdate2.get():
                tk.messagebox.showerror("Update Error", "New Passwords Do Not Match")
                pWordUpdate.delete(0, 'end')
                pWordUpdate2.delete(0, 'end')
            else:
                connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='users')
                cursordb = connectiondb.cursor()
                cursordb.execute("UPDATE users SET Password=%s WHERE Username=%s", [(pWordUpdate.get()), (user_name)])
                connectiondb.commit()
                connectiondb.close()
                pWordUpdate.delete(0, 'end')
                pWordUpdate2.delete(0, 'end')
                tk.messagebox.showinfo("Update User", "Password Updated Successfully")

        self.configure(bg="#CFF6FF")

        scrWidth = self.winfo_screenwidth()
        scrHeight = self.winfo_screenheight()
        ctr = (scrWidth/2)-150

        logo_small(self, parent, root)
        student_acc_op_page(self, parent, root)

        pWordUpdate = Entry(self, bg="#FFFFFF", width=37, show="*")
        pWordUpdate.place(x=ctr, y=450)

        pWordUpdate2 = Entry(self, bg="#FFFFFF", width=37, show="*")
        pWordUpdate2.place(x=ctr, y=525)

        UpdateBut = Button(self, text="Update Password", width=15, command=lambda: update_student_user)
        UpdateBut.configure(bg="#83B4B3", activebackground="#097392")
        UpdateBut.place(x=ctr+75, y=575)

        logoutBut = Button(self, text="Logout", width=15, command=lambda: root.show_frame(loginPage))
        logoutBut.configure(bg="#83B4B3", activebackground="#097392")
        logoutBut.place(x=scrWidth-185, y=25)

        helpBut = Button(self, text="Help", width=15, command=lambda: root.show_frame(studentHelpPage))
        helpBut.configure(bg="#83B4B3", activebackground="#097392")
        helpBut.place(x=scrWidth-360, y=25)

        backBut = Button(self, text="Back", width=15, command=lambda: self.lower())
        backBut.configure(bg="#83B4B3", activebackground="#097392")
        backBut.place(x=scrWidth-185, y=scrHeight-150)

class staffLandingPage(tk.Frame):                   # Class for the staff landing page
    def __init__(self, parent, root):
        super().__init__(parent)

        self.configure(bg="#B6F1FF")

        scrWidth = self.winfo_screenwidth()

        logo_small(self, parent, root)

        logoutBut = Button(self, text="Logout", width=15, command=lambda: root.show_frame(loginPage))
        logoutBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        logoutBut.place(x=scrWidth-185, y=25)

        helpBut = Button(self, text="Help", width=15, command=lambda: root.show_frame(staffHelpPage))
        helpBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        helpBut.place(x=scrWidth-360, y=25)

        staffAccOpBut = Button(self, text="Account Options", width=15)
        staffAccOpBut.configure(bg="#83B4B3", activebackground="#CFF6FF", command=lambda: root.show_frame(staffAccountOpPage))
        staffAccOpBut.place(x=scrWidth-535, y=25)

        rmgBut = Button(self, text="Risk Management and Governance", width=23, height=7, wraplength=200, command=lambda: root.show_frame(staffRMandG))
        rmgBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        rmgBut.place(x=25, y=275)
        lrBut = Button(self, text="Law and Regulation", width=23, height=7, wraplength=200, command=lambda: root.show_frame(staffLawandReg))
        lrBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        lrBut.place(x=300, y=275)
        hfBut = Button(self, text="Human Factors", width=23, height=7, wraplength=200, command=lambda: root.show_frame(staffHumanFactors))
        hfBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        hfBut.place(x=575, y=275)
        porBut = Button(self, text="Privacy and Online Rights", width=23, height=7, wraplength=200, command=lambda: root.show_frame(staffPandOR))
        porBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        porBut.place(x=850, y=275)
        matBut = Button(self, text="Malware and Attack Technologies", width=23, height=7, wraplength=200, command=lambda: root.show_frame(staffMandAT))
        matBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        matBut.place(x=1125, y=275)
        abBut = Button(self, text="Adversarial Behaviours", width=23, height=7, wraplength=200, command=lambda: root.show_frame(staffAdversarialBehaviours))
        abBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        abBut.place(x=1400, y=275)
        soimBut = Button(self, text="Security Operations and Incident Management", width=23, height=7, wraplength=200, command=lambda: root.show_frame(staffSOandIM))
        soimBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        soimBut.place(x=1675, y=275)
        fBut = Button(self, text="Forensics", width=23, height=7, wraplength=200, command=lambda: root.show_frame(staffForensics))
        fBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        fBut.place(x=1950, y=275)
        cBut = Button(self, text="Cryptography", width=23, height=7, wraplength=200, command=lambda: root.show_frame(staffCryptography))
        cBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        cBut.place(x=25, y=450)
        osvsBut = Button(self, text="Operating Systems and Virtualisation Security", width=23, height=7, wraplength=200, command=lambda: root.show_frame(staffOSandVS))
        osvsBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        osvsBut.place(x=300, y=450)
        dssBut = Button(self, text="Distributed Systems Security", width=23, height=7, wraplength=200, command=lambda: root.show_frame(staffDSS))
        dssBut.configure(bg="#83B4B3", activebackground="#097392")
        dssBut.place(x=575, y=450)
        aaaBut = Button(self, text="Authentication, Authorisation and Accountability", width=23, height=7, wraplength=200, command=lambda: root.show_frame(staffAAandA))
        aaaBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        aaaBut.place(x=850, y=450)
        ssBut = Button(self, text="Software Security", width=23, height=7, wraplength=200, command=lambda: root.show_frame(staffSoftwareSecurity))
        ssBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        ssBut.place(x=1125, y=450)
        wmsBut = Button(self, text="Web and Mobile Security", width=23, height=7, wraplength=200, command=lambda: root.show_frame(staffWandMS))
        wmsBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        wmsBut.place(x=1400, y=450)
        sslBut = Button(self, text="Secure Software Lifecycle", width=23, height=7, wraplength=200, command=lambda: root.show_frame(staffSSL))
        sslBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        sslBut.place(x=1675, y=450)
        nsBut = Button(self, text="Network Security", width=23, height=7, wraplength=200, command=lambda: root.show_frame(staffNetworkSecurity))
        nsBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        nsBut.place(x=1950, y=450)
        hsBut = Button(self, text="Hardware Security", width=23, height=7, wraplength=200, command=lambda: root.show_frame(staffHardwareSecurity))
        hsBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        hsBut.place(x=25, y=625)
        cpsBut = Button(self, text="Cyber Physical Systems", width=23, height=7, wraplength=200, command=lambda: root.show_frame(staffCyberPhysicalSystems))
        cpsBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        cpsBut.place(x=300, y=625)
        pltsBut = Button(self, text="Physical Layer and Telecommunications Security", width=23, height=7, wraplength=200, command=lambda: root.show_frame(staffPLandTS))
        pltsBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        pltsBut.place(x=575, y=625)

class staffHelpPage(tk.Frame):                      # Class for the staff help page
    def __init__(self, parent, root):
        super().__init__(parent)

        self.configure(bg="#B6F1FF")

        scrWidth = self.winfo_screenwidth()
        scrHeight = self.winfo_screenheight()

        logo_small(self, parent, root)
        staff_help_page(self, parent, root)

        backBut = Button(self, text="Back", width=15, command=lambda: self.lower())
        backBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        backBut.place(x=scrWidth-185, y=scrHeight-150)

class staffAccountOpPage(tk.Frame):                 # Class for the staff account page
    def __init__(self, parent, root):
        super().__init__(parent)

        global user_name

        def add_admin_user():                       # Func to add a new admin user to the database
            if uName.get() == "" or pWord.get() == "":
                tk.messagebox.showerror("MySQL Connection", "Enter details in all fields")
                uName.delete(0, 'end')
                pWord.delete(0, 'end')
            else:
                try:
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='users')
                    cursordb = connectiondb.cursor()
                    cursordb.execute("INSERT INTO users VALUES(%s,%s,%s)", (uName.get(), pWord.get(), "Admin"))
                    connectiondb.commit()
                    connectiondb.close()
                    uName.delete(0, 'end')
                    pWord.delete(0, 'end')
                    tk.messagebox.showinfo("MySQL Connection", "New Admin Created")
                except:
                    tk.messagebox.showerror("MySQL Connection", "Error: Username Already In Use")

        def update_admin_user():                    # Func to update the current admin users password
            global user_name
            if pWordUpdate.get() == "" or pWordUpdate2.get() == "":
                tk.messagebox.showerror("Update Error", "Please enter details in both fields")
                pWordUpdate.delete(0, 'end')
                pWordUpdate2.delete(0, 'end')
            elif pWordUpdate.get() != pWordUpdate2.get():
                tk.messagebox.showerror("Update Error", "New Passwords Do Not Match")
                pWordUpdate.delete(0, 'end')
                pWordUpdate2.delete(0, 'end')
            else:
                connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='users')
                cursordb = connectiondb.cursor()
                cursordb.execute("UPDATE users SET Password=%s WHERE Username=%s", [(pWordUpdate.get()), (user_name)])
                connectiondb.commit()
                connectiondb.close()
                pWordUpdate.delete(0, 'end')
                pWordUpdate2.delete(0, 'end')
                tk.messagebox.showinfo("Update User", "Password Updated Successfully")

        self.configure(bg="#B6F1FF")

        scrWidth = self.winfo_screenwidth()
        scrHeight = self.winfo_screenheight()
        ctr = (scrWidth/2)-150

        logo_small(self, parent, root)
        staff_acc_op_page(self, parent, root)

        pWordUpdate = Entry(self, bg="#FFFFFF", width=37, show="*")
        pWordUpdate.place(x=ctr, y=325)

        pWordUpdate2 = Entry(self, bg="#FFFFFF", width=37, show="*")
        pWordUpdate2.place(x=ctr, y=400)

        UpdateBut = Button(self, text="Update Password", width=15, command=update_admin_user)
        UpdateBut.configure(bg="#83B4B3", activebackground="#097392")
        UpdateBut.place(x=ctr+75, y=450)

        uName = Entry(self, bg="#FFFFFF", width=37)
        uName.place(x=ctr, y=550)

        pWord = Entry(self, bg="#FFFFFF", width=37, show="*")
        pWord.place(x=ctr, y=625)

        loginBut = Button(self, text="Create New Admin", width=15, command=add_admin_user)
        loginBut.configure(bg="#83B4B3", activebackground="#097392")
        loginBut.place(x=ctr+75, y=675)

        logoutBut = Button(self, text="Logout", width=15, command=lambda: root.show_frame(loginPage))
        logoutBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        logoutBut.place(x=scrWidth-185, y=25)

        helpBut = Button(self, text="Help", width=15, command=lambda: root.show_frame(staffHelpPage))
        helpBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        helpBut.place(x=scrWidth-360, y=25)

        backBut = Button(self, text="Back", width=15, command=lambda: self.lower())
        backBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        backBut.place(x=scrWidth-185, y=scrHeight-150)

class staffRMandG(tk.Frame):                        # Class for the staff risk management and governance page
    def __init__(self, parent, root):
        super().__init__(parent)

        def add_file():                             # Func to add a file to the pages database table
            if fileName.get() == "":
                tk.messagebox.showerror("File Name Error", "Please enter the name you have chosen for the file you are uploading")
            else:
                try:
                    file = filedialog.askopenfilename(initialdir="/home/pi", title="Select file to add", filetypes=(("All Files", "*.*"), ("Python files", "*.py")))
                    fName = fileName.get()
                    with open(file, "rb") as f:
                        filedata = f.read()
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                    cursordb = connectiondb.cursor()
                    sql = "INSERT INTO riskmanagementandgovernance(name, filedata) VALUES(%s, %s)"
                    cursordb.execute(sql, (fName, filedata))
                    tk.messagebox.showinfo("Success", "File successfully uploaded")
                    fileName.delete(0, 'end')
                    connectiondb.commit()
                    connectiondb.close()
                except:
                    tk.messagebox.showerror("File Error", "Error: File Name Already In Use")

        def remove_file():                          # Func to remove a file from the pages database table
            if fileName.get() == "":
                tk.messagebox.showerror("File Name Error", "Please enter the name of the file you wish to remove")
            else:
                try:
                    fName = fileName.get()
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                    cursordb = connectiondb.cursor()
                    cursordb.execute("DELETE FROM riskmanagementandgovernance WHERE name = %s", [(fName)])
                    tk.messagebox.showinfo("Success", "File successfully deleted")
                    fileName.delete(0, 'end')
                    connectiondb.commit()
                    connectiondb.close()
                except:
                    tk.messagebox.showerror("File Error", "Error: File Not Found")

        def update_file():                          # Func to update a file within the pages database table
            if fileName.get() == "":
                tk.messagebox.showerror("File Name Error", "Please enter the name of the file you wish to update")
            else:
                try:
                    file = filedialog.askopenfilename(initialdir="/home/pi", title="Select file to add", filetypes=(("All Files", "*.*"), ("Exe files", "*.exe"), ("PDF files", "*.pdf")))
                    fName = fileName.get()
                    with open(file, "rb") as f:
                        filedata = f.read()
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                    cursordb = connectiondb.cursor()
                    cursordb.execute("UPDATE riskmanagementandgovernance SET filedata = %s WHERE name = %s", [(filedata), (fName)])
                    tk.messagebox.showinfo("Success", "File successfully updated")
                    fileName.delete(0, 'end')
                    connectiondb.commit()
                    connectiondb.close()
                except:
                    tk.messagebox.showerror("File Error", "Error: File Not Found")

        def run_file(fName):                        # Func to run the file selected from within the database
                connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                cursordb = connectiondb.cursor()
                cursordb.execute("SELECT filedata FROM riskmanagementandgovernance WHERE name = %s", [(fName)])
                r = cursordb.fetchall()
                for i in r:
                    data = i[0]
                decodedData = data.decode("utf8")
                exec(decodedData)
                connectiondb.commit()
                connectiondb.close()

        def check_files():                          # Func to list and allow running of files in the database table
            connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
            cursordb = connectiondb.cursor()
            cursordb.execute("SELECT name FROM riskmanagementandgovernance")
            r = cursordb.fetchall()
            connectiondb.commit()
            connectiondb.close()
            startX = 25
            startY = 275
            j = 0
            for i in r:
                button = Button(self, text=i, width=15)
                button.configure(bg="#83B4B3", activebackground="#CFF6FF", command=lambda: run_file(i[0]))
                button.place(x=startX, y=startY)
                startX = startX+200
                j = j+1
                if j == 6:
                    startY = startY+100
                    startX = 25
                    j = 0

        self.configure(bg="#B6F1FF")

        scrWidth = self.winfo_screenwidth()
        scrHeight = self.winfo_screenheight()
        check_files()

        logo_small(self, parent, root)

        logoutBut = Button(self, text="Logout", width=15, command=lambda: root.show_frame(loginPage))
        logoutBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        logoutBut.place(x=scrWidth-185, y=25)

        helpBut = Button(self, text="Help", width=15, command=lambda: root.show_frame(staffHelpPage))
        helpBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        helpBut.place(x=scrWidth-360, y=25)

        staffAccOpBut = Button(self, text="Account Options", width=15)
        staffAccOpBut.configure(bg="#83B4B3", activebackground="#CFF6FF", command=lambda: root.show_frame(staffAccountOpPage))
        staffAccOpBut.place(x=scrWidth-535, y=25)

        backBut = Button(self, text="Back", width=15, command=lambda: [self.lower(), check_files()])
        backBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        backBut.place(x=scrWidth-185, y=scrHeight-150)

        updateBut = Button(self, text="Update", width=15, command=lambda: [update_file(), check_files()])
        updateBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        updateBut.place(x=scrWidth-360, y=scrHeight-150)

        removeBut = Button(self, text="Remove", width=15, command=lambda: [remove_file(), check_files()])
        removeBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        removeBut.place(x=scrWidth-535, y=scrHeight-150)

        addBut = Button(self, text="Add", width=15, command=lambda: [add_file(), check_files()])
        addBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        addBut.place(x=scrWidth-710, y=scrHeight-150)

        fileName = Entry(self, bg="#FFFFFF", width=37)
        fileName.place(x=scrWidth-1040, y=scrHeight-150, height=31)

        file_label(self, parent, root)

class staffLawandReg(tk.Frame):

    def __init__(self, parent, root):
        super().__init__(parent)

        def add_file():
            if fileName.get() == "":
                tk.messagebox.showerror("File Name Error", "Please enter the name you have chosen for the file you are uploading")
            else:
                try:
                    file = filedialog.askopenfilename(initialdir="/home/pi", title="Select file to add", filetypes=(("All Files", "*.*"), ("Python files", "*.py")))
                    fName = fileName.get()
                    with open(file, "rb") as f:
                        filedata = f.read()
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                    cursordb = connectiondb.cursor()
                    sql = "INSERT INTO lawandregulation(name, filedata) VALUES(%s, %s)"
                    cursordb.execute(sql, (fName, filedata))
                    tk.messagebox.showinfo("Success", "File successfully uploaded")
                    fileName.delete(0, 'end')
                    connectiondb.commit()
                    connectiondb.close()
                except:
                    tk.messagebox.showerror("File Error", "Error: Filename already in use")

        def remove_file():
            if fileName.get() == "":
                tk.messagebox.showerror("File Name Error", "Please enter the name of the file you wish to remove")
            else:
                try:
                    fName = fileName.get()
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                    cursordb = connectiondb.cursor()
                    cursordb.execute("DELETE FROM lawandregulation WHERE name = %s", [(fName)])
                    tk.messagebox.showinfo("Success", "File successfully deleted")
                    fileName.delete(0, 'end')
                    connectiondb.commit()
                    connectiondb.close()
                except:
                    tk.messagebox.showerror("File Error", "Error: File Not Found")

        def update_file():
            if fileName.get() == "":
                tk.messagebox.showerror("File Name Error", "Please enter the name of the file you wish to update")
            else:
                try:
                    file = filedialog.askopenfilename(initialdir="/home/pi", title="Select file to add", filetypes=(("All Files", "*.*"), ("Exe files", "*.exe"), ("PDF files", "*.pdf")))
                    fName = fileName.get()
                    with open(file, "rb") as f:
                        filedata = f.read()
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                    cursordb = connectiondb.cursor()
                    cursordb.execute("UPDATE lawandregulation SET filedata = %s WHERE name = %s", [(filedata), (fName)])
                    tk.messagebox.showinfo("Success", "File successfully updated")
                    fileName.delete(0, 'end')
                    connectiondb.commit()
                    connectiondb.close()
                except:
                    tk.messagebox.showerror("File Error", "Error: File Not Found")

        def run_file(fName):
                connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                cursordb = connectiondb.cursor()
                cursordb.execute("SELECT filedata FROM lawandregulation WHERE name = %s", [(fName)])
                r = cursordb.fetchall()
                for i in r:
                    data = i[0]
                decodedData = data.decode("utf8")
                exec(decodedData)
                connectiondb.commit()
                connectiondb.close()

        def check_files():
            connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
            cursordb = connectiondb.cursor()
            cursordb.execute("SELECT name FROM lawandregulation")
            r = cursordb.fetchall()
            connectiondb.commit()
            connectiondb.close()
            startX = 25
            startY = 275
            j = 0
            for i in r:
                button = Button(self, text=i, width=15)
                button.configure(bg="#83B4B3", activebackground="#CFF6FF", command=lambda: run_file(i[0]))
                button.place(x=startX, y=startY)
                startX = startX+200
                j = j+1
                if j == 6:
                    startY = startY+100
                    startX = 25
                    j = 0

        self.configure(bg="#B6F1FF")

        scrWidth = self.winfo_screenwidth()
        scrHeight = self.winfo_screenheight()
        check_files()

        logo_small(self, parent, root)

        # Configure logout button
        logoutBut = Button(self, text="Logout", width=15, command=lambda: root.show_frame(loginPage))
        logoutBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        logoutBut.place(x=scrWidth-185, y=25)

        # Configure help button
        helpBut = Button(self, text="Help", width=15, command=lambda: root.show_frame(staffHelpPage))
        helpBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        helpBut.place(x=scrWidth-360, y=25)

        # Configure account options button
        staffAccOpBut = Button(self, text="Account Options", width=15)
        staffAccOpBut.configure(bg="#83B4B3", activebackground="#CFF6FF", command=lambda: root.show_frame(staffAccountOpPage))
        staffAccOpBut.place(x=scrWidth-535, y=25)

        # Configure button to go back to staff landing page
        backBut = Button(self, text="Back", width=15, command=lambda: [self.lower(), check_files()])
        backBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        backBut.place(x=scrWidth-185, y=scrHeight-150)

        # Configure update button
        updateBut = Button(self, text="Update", width=15, command=lambda: [update_file(), check_files()])
        updateBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        updateBut.place(x=scrWidth-360, y=scrHeight-150)

        # Configure remove button
        removeBut = Button(self, text="Remove", width=15, command=lambda: [remove_file(), check_files()])
        removeBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        removeBut.place(x=scrWidth-535, y=scrHeight-150)

        # Configure add button
        addBut = Button(self, text="Add", width=15, command=lambda: [add_file(), check_files()])
        addBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        addBut.place(x=scrWidth-710, y=scrHeight-150)

        # Configure name entry
        fileName = Entry(self, bg="#FFFFFF", width=37)
        fileName.place(x=scrWidth-1040, y=scrHeight-150, height=31)

        file_label(self, parent, root)

class staffHumanFactors(tk.Frame):

    def __init__(self, parent, root):
        super().__init__(parent)

        def add_file():
            if fileName.get() == "":
                tk.messagebox.showerror("File Name Error", "Please enter the name you have chosen for the file you are uploading")
            else:
                try:
                    file = filedialog.askopenfilename(initialdir="/home/pi", title="Select file to add", filetypes=(("All Files", "*.*"), ("Python files", "*.py")))
                    fName = fileName.get()
                    with open(file, "rb") as f:
                        filedata = f.read()
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                    cursordb = connectiondb.cursor()
                    sql = "INSERT INTO humanfactors(name, filedata) VALUES(%s, %s)"
                    cursordb.execute(sql, (fName, filedata))
                    tk.messagebox.showinfo("Success", "File successfully uploaded")
                    fileName.delete(0, 'end')
                    connectiondb.commit()
                    connectiondb.close()
                except:
                    tk.messagebox.showerror("File Error", "Error: Filename already in use")

        def remove_file():
            if fileName.get() == "":
                tk.messagebox.showerror("File Name Error", "Please enter the name of the file you wish to remove")
            else:
                try:
                    fName = fileName.get()
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                    cursordb = connectiondb.cursor()
                    cursordb.execute("DELETE FROM humanfactors WHERE name = %s", [(fName)])
                    tk.messagebox.showinfo("Success", "File successfully deleted")
                    fileName.delete(0, 'end')
                    connectiondb.commit()
                    connectiondb.close()
                except:
                    tk.messagebox.showerror("File Error", "Error: File Not Found")

        def update_file():
            if fileName.get() == "":
                tk.messagebox.showerror("File Name Error", "Please enter the name of the file you wish to update")
            else:
                try:
                    file = filedialog.askopenfilename(initialdir="/home/pi", title="Select file to add", filetypes=(("All Files", "*.*"), ("Exe files", "*.exe"), ("PDF files", "*.pdf")))
                    fName = fileName.get()
                    with open(file, "rb") as f:
                        filedata = f.read()
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                    cursordb = connectiondb.cursor()
                    cursordb.execute("UPDATE humanfactors SET filedata = %s WHERE name = %s", [(filedata), (fName)])
                    tk.messagebox.showinfo("Success", "File successfully updated")
                    fileName.delete(0, 'end')
                    connectiondb.commit()
                    connectiondb.close()
                except:
                    tk.messagebox.showerror("File Error", "Error: File Not Found")

        def run_file(fName):
                connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                cursordb = connectiondb.cursor()
                cursordb.execute("SELECT filedata FROM humanfactors WHERE name = %s", [(fName)])
                r = cursordb.fetchall()
                for i in r:
                    data = i[0]
                decodedData = data.decode("utf8")
                exec(decodedData)
                connectiondb.commit()
                connectiondb.close()

        def check_files():
            connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
            cursordb = connectiondb.cursor()
            cursordb.execute("SELECT name FROM humanfactors")
            r = cursordb.fetchall()
            connectiondb.commit()
            connectiondb.close()
            startX = 25
            startY = 275
            j = 0
            for i in r:
                button = Button(self, text=i, width=15)
                button.configure(bg="#83B4B3", activebackground="#CFF6FF", command=lambda: run_file(i[0]))
                button.place(x=startX, y=startY)
                startX = startX+200
                j = j+1
                if j == 6:
                    startY = startY+100
                    startX = 25
                    j = 0

        self.configure(bg="#B6F1FF")

        scrWidth = self.winfo_screenwidth()
        scrHeight = self.winfo_screenheight()
        check_files()

        logo_small(self, parent, root)

        # Configure logout button
        logoutBut = Button(self, text="Logout", width=15, command=lambda: root.show_frame(loginPage))
        logoutBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        logoutBut.place(x=scrWidth-185, y=25)

        # Configure help button
        helpBut = Button(self, text="Help", width=15, command=lambda: root.show_frame(staffHelpPage))
        helpBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        helpBut.place(x=scrWidth-360, y=25)

        # Configure account options button
        staffAccOpBut = Button(self, text="Account Options", width=15)
        staffAccOpBut.configure(bg="#83B4B3", activebackground="#CFF6FF", command=lambda: root.show_frame(staffAccountOpPage))
        staffAccOpBut.place(x=scrWidth-535, y=25)

        # Configure button to go back to staff landing page
        backBut = Button(self, text="Back", width=15, command=lambda: [self.lower(), check_files()])
        backBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        backBut.place(x=scrWidth-185, y=scrHeight-150)

        # Configure update button
        updateBut = Button(self, text="Update", width=15, command=lambda: [update_file(), check_files()])
        updateBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        updateBut.place(x=scrWidth-360, y=scrHeight-150)

        # Configure remove button
        removeBut = Button(self, text="Remove", width=15, command=lambda: [remove_file(), check_files()])
        removeBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        removeBut.place(x=scrWidth-535, y=scrHeight-150)

        # Configure add button
        addBut = Button(self, text="Add", width=15, command=lambda: [add_file(), check_files()])
        addBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        addBut.place(x=scrWidth-710, y=scrHeight-150)

        # Configure name entry
        fileName = Entry(self, bg="#FFFFFF", width=37)
        fileName.place(x=scrWidth-1040, y=scrHeight-150, height=31)

        file_label(self, parent, root)

class staffPandOR(tk.Frame):

    def __init__(self, parent, root):
        super().__init__(parent)

        def add_file():
            if fileName.get() == "":
                tk.messagebox.showerror("File Name Error", "Please enter the name you have chosen for the file you are uploading")
            else:
                try:
                    file = filedialog.askopenfilename(initialdir="/home/pi", title="Select file to add", filetypes=(("All Files", "*.*"), ("Python files", "*.py")))
                    fName = fileName.get()
                    with open(file, "rb") as f:
                        filedata = f.read()
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                    cursordb = connectiondb.cursor()
                    sql = "INSERT INTO privacyandonlinerights(name, filedata) VALUES(%s, %s)"
                    cursordb.execute(sql, (fName, filedata))
                    tk.messagebox.showinfo("Success", "File successfully uploaded")
                    fileName.delete(0, 'end')
                    connectiondb.commit()
                    connectiondb.close()
                except:
                    tk.messagebox.showerror("File Error", "Error: Filename already in use")

        def remove_file():
            if fileName.get() == "":
                tk.messagebox.showerror("File Name Error", "Please enter the name of the file you wish to remove")
            else:
                try:
                    fName = fileName.get()
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                    cursordb = connectiondb.cursor()
                    cursordb.execute("DELETE FROM privacyandonlinerights WHERE name = %s", [(fName)])
                    tk.messagebox.showinfo("Success", "File successfully deleted")
                    fileName.delete(0, 'end')
                    connectiondb.commit()
                    connectiondb.close()
                except:
                    tk.messagebox.showerror("File Error", "Error: File Not Found")

        def update_file():
            if fileName.get() == "":
                tk.messagebox.showerror("File Name Error", "Please enter the name of the file you wish to update")
            else:
                try:
                    file = filedialog.askopenfilename(initialdir="/home/pi", title="Select file to add", filetypes=(("All Files", "*.*"), ("Exe files", "*.exe"), ("PDF files", "*.pdf")))
                    fName = fileName.get()
                    with open(file, "rb") as f:
                        filedata = f.read()
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                    cursordb = connectiondb.cursor()
                    cursordb.execute("UPDATE privacyandonlinerights SET filedata = %s WHERE name = %s", [(filedata), (fName)])
                    tk.messagebox.showinfo("Success", "File successfully updated")
                    fileName.delete(0, 'end')
                    connectiondb.commit()
                    connectiondb.close()
                except:
                    tk.messagebox.showerror("File Error", "Error: File Not Found")

        def run_file(fName):
                connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                cursordb = connectiondb.cursor()
                cursordb.execute("SELECT filedata FROM privacyandonlinerights WHERE name = %s", [(fName)])
                r = cursordb.fetchall()
                for i in r:
                    data = i[0]
                decodedData = data.decode("utf8")
                exec(decodedData)
                connectiondb.commit()
                connectiondb.close()

        def check_files():
            connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
            cursordb = connectiondb.cursor()
            cursordb.execute("SELECT name FROM privacyandonlinerights")
            r = cursordb.fetchall()
            connectiondb.commit()
            connectiondb.close()
            startX = 25
            startY = 275
            j = 0
            for i in r:
                button = Button(self, text=i, width=15)
                button.configure(bg="#83B4B3", activebackground="#CFF6FF", command=lambda: run_file(i[0]))
                button.place(x=startX, y=startY)
                startX = startX+200
                j = j+1
                if j == 6:
                    startY = startY+100
                    startX = 25
                    j = 0

        self.configure(bg="#B6F1FF")

        scrWidth = self.winfo_screenwidth()
        scrHeight = self.winfo_screenheight()
        check_files()

        logo_small(self, parent, root)

        # Configure logout button
        logoutBut = Button(self, text="Logout", width=15, command=lambda: root.show_frame(loginPage))
        logoutBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        logoutBut.place(x=scrWidth-185, y=25)

        # Configure help button
        helpBut = Button(self, text="Help", width=15, command=lambda: root.show_frame(staffHelpPage))
        helpBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        helpBut.place(x=scrWidth-360, y=25)

        # Configure account options button
        staffAccOpBut = Button(self, text="Account Options", width=15)
        staffAccOpBut.configure(bg="#83B4B3", activebackground="#CFF6FF", command=lambda: root.show_frame(staffAccountOpPage))
        staffAccOpBut.place(x=scrWidth-535, y=25)

        # Configure button to go back to staff landing page
        backBut = Button(self, text="Back", width=15, command=lambda: [self.lower(), check_files()])
        backBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        backBut.place(x=scrWidth-185, y=scrHeight-150)

        # Configure update button
        updateBut = Button(self, text="Update", width=15, command=lambda: [update_file(), check_files()])
        updateBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        updateBut.place(x=scrWidth-360, y=scrHeight-150)

        # Configure remove button
        removeBut = Button(self, text="Remove", width=15, command=lambda: [remove_file(), check_files()])
        removeBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        removeBut.place(x=scrWidth-535, y=scrHeight-150)

        # Configure add button
        addBut = Button(self, text="Add", width=15, command=lambda: [add_file(), check_files()])
        addBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        addBut.place(x=scrWidth-710, y=scrHeight-150)

        # Configure name entry
        fileName = Entry(self, bg="#FFFFFF", width=37)
        fileName.place(x=scrWidth-1040, y=scrHeight-150, height=31)

        file_label(self, parent, root)

class staffMandAT(tk.Frame):

    def __init__(self, parent, root):
        super().__init__(parent)

        def add_file():
            if fileName.get() == "":
                tk.messagebox.showerror("File Name Error", "Please enter the name you have chosen for the file you are uploading")
            else:
                try:
                    file = filedialog.askopenfilename(initialdir="/home/pi", title="Select file to add", filetypes=(("All Files", "*.*"), ("Python files", "*.py")))
                    fName = fileName.get()
                    with open(file, "rb") as f:
                        filedata = f.read()
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                    cursordb = connectiondb.cursor()
                    sql = "INSERT INTO malwareandattacktechnologies(name, filedata) VALUES(%s, %s)"
                    cursordb.execute(sql, (fName, filedata))
                    tk.messagebox.showinfo("Success", "File successfully uploaded")
                    fileName.delete(0, 'end')
                    connectiondb.commit()
                    connectiondb.close()
                except:
                    tk.messagebox.showerror("File Error", "Error: Filename already in use")

        def remove_file():
            if fileName.get() == "":
                tk.messagebox.showerror("File Name Error", "Please enter the name of the file you wish to remove")
            else:
                try:
                    fName = fileName.get()
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                    cursordb = connectiondb.cursor()
                    cursordb.execute("DELETE FROM malwareandattacktechnologies WHERE name = %s", [(fName)])
                    tk.messagebox.showinfo("Success", "File successfully deleted")
                    fileName.delete(0, 'end')
                    connectiondb.commit()
                    connectiondb.close()
                except:
                    tk.messagebox.showerror("File Error", "Error: File Not Found")

        def update_file():
            if fileName.get() == "":
                tk.messagebox.showerror("File Name Error", "Please enter the name of the file you wish to update")
            else:
                try:
                    file = filedialog.askopenfilename(initialdir="/home/pi", title="Select file to add", filetypes=(("All Files", "*.*"), ("Exe files", "*.exe"), ("PDF files", "*.pdf")))
                    fName = fileName.get()
                    with open(file, "rb") as f:
                        filedata = f.read()
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                    cursordb = connectiondb.cursor()
                    cursordb.execute("UPDATE malwareandattacktechnologies SET filedata = %s WHERE name = %s", [(filedata), (fName)])
                    tk.messagebox.showinfo("Success", "File successfully updated")
                    fileName.delete(0, 'end')
                    connectiondb.commit()
                    connectiondb.close()
                except:
                    tk.messagebox.showerror("File Error", "Error: File Not Found")

        def run_file(fName):
                connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                cursordb = connectiondb.cursor()
                cursordb.execute("SELECT filedata FROM malwareandattacktechnologies WHERE name = %s", [(fName)])
                r = cursordb.fetchall()
                for i in r:
                    data = i[0]
                decodedData = data.decode("utf8")
                exec(decodedData)
                connectiondb.commit()
                connectiondb.close()

        def check_files():
            connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
            cursordb = connectiondb.cursor()
            cursordb.execute("SELECT name FROM malwareandattacktechnologies")
            r = cursordb.fetchall()
            connectiondb.commit()
            connectiondb.close()
            startX = 25
            startY = 275
            j = 0
            for i in r:
                button = Button(self, text=i, width=15)
                button.configure(bg="#83B4B3", activebackground="#CFF6FF", command=lambda: run_file(i[0]))
                button.place(x=startX, y=startY)
                startX = startX+200
                j = j+1
                if j == 6:
                    startY = startY+100
                    startX = 25
                    j = 0

        self.configure(bg="#B6F1FF")

        scrWidth = self.winfo_screenwidth()
        scrHeight = self.winfo_screenheight()
        check_files()

        logo_small(self, parent, root)

        # Configure logout button
        logoutBut = Button(self, text="Logout", width=15, command=lambda: root.show_frame(loginPage))
        logoutBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        logoutBut.place(x=scrWidth-185, y=25)

        # Configure help button
        helpBut = Button(self, text="Help", width=15, command=lambda: root.show_frame(staffHelpPage))
        helpBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        helpBut.place(x=scrWidth-360, y=25)

        # Configure account options button
        staffAccOpBut = Button(self, text="Account Options", width=15)
        staffAccOpBut.configure(bg="#83B4B3", activebackground="#CFF6FF", command=lambda: root.show_frame(staffAccountOpPage))
        staffAccOpBut.place(x=scrWidth-535, y=25)

        # Configure button to go back to staff landing page
        backBut = Button(self, text="Back", width=15, command=lambda: [self.lower(), check_files()])
        backBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        backBut.place(x=scrWidth-185, y=scrHeight-150)

        # Configure update button
        updateBut = Button(self, text="Update", width=15, command=lambda: [update_file(), check_files()])
        updateBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        updateBut.place(x=scrWidth-360, y=scrHeight-150)

        # Configure remove button
        removeBut = Button(self, text="Remove", width=15, command=lambda: [remove_file(), check_files()])
        removeBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        removeBut.place(x=scrWidth-535, y=scrHeight-150)

        # Configure add button
        addBut = Button(self, text="Add", width=15, command=lambda: [add_file(), check_files()])
        addBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        addBut.place(x=scrWidth-710, y=scrHeight-150)

        # Configure name entry
        fileName = Entry(self, bg="#FFFFFF", width=37)
        fileName.place(x=scrWidth-1040, y=scrHeight-150, height=31)

        file_label(self, parent, root)

class staffAdversarialBehaviours(tk.Frame):

    def __init__(self, parent, root):
        super().__init__(parent)

        def add_file():
            if fileName.get() == "":
                tk.messagebox.showerror("File Name Error", "Please enter the name you have chosen for the file you are uploading")
            else:
                try:
                    file = filedialog.askopenfilename(initialdir="/home/pi", title="Select file to add", filetypes=(("All Files", "*.*"), ("Python files", "*.py")))
                    fName = fileName.get()
                    with open(file, "rb") as f:
                        filedata = f.read()
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                    cursordb = connectiondb.cursor()
                    sql = "INSERT INTO adversarialbehaviours(name, filedata) VALUES(%s, %s)"
                    cursordb.execute(sql, (fName, filedata))
                    tk.messagebox.showinfo("Success", "File successfully uploaded")
                    fileName.delete(0, 'end')
                    connectiondb.commit()
                    connectiondb.close()
                except:
                    tk.messagebox.showerror("File Error", "Error: Filename already in use")

        def remove_file():
            if fileName.get() == "":
                tk.messagebox.showerror("File Name Error", "Please enter the name of the file you wish to remove")
            else:
                try:
                    fName = fileName.get()
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                    cursordb = connectiondb.cursor()
                    cursordb.execute("DELETE FROM adversarialbehaviours WHERE name = %s", [(fName)])
                    tk.messagebox.showinfo("Success", "File successfully deleted")
                    fileName.delete(0, 'end')
                    connectiondb.commit()
                    connectiondb.close()
                except:
                    tk.messagebox.showerror("File Error", "Error: File Not Found")

        def update_file():
            if fileName.get() == "":
                tk.messagebox.showerror("File Name Error", "Please enter the name of the file you wish to update")
            else:
                try:
                    file = filedialog.askopenfilename(initialdir="/home/pi", title="Select file to add", filetypes=(("All Files", "*.*"), ("Exe files", "*.exe"), ("PDF files", "*.pdf")))
                    fName = fileName.get()
                    with open(file, "rb") as f:
                        filedata = f.read()
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                    cursordb = connectiondb.cursor()
                    cursordb.execute("UPDATE adversarialbehaviours SET filedata = %s WHERE name = %s", [(filedata), (fName)])
                    tk.messagebox.showinfo("Success", "File successfully updated")
                    fileName.delete(0, 'end')
                    connectiondb.commit()
                    connectiondb.close()
                except:
                    tk.messagebox.showerror("File Error", "Error: File Not Found")

        def run_file(fName):
                connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                cursordb = connectiondb.cursor()
                cursordb.execute("SELECT filedata FROM adversarialbehaviours WHERE name = %s", [(fName)])
                r = cursordb.fetchall()
                for i in r:
                    data = i[0]
                decodedData = data.decode("utf8")
                exec(decodedData)
                connectiondb.commit()
                connectiondb.close()

        def check_files():
            connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
            cursordb = connectiondb.cursor()
            cursordb.execute("SELECT name FROM adversarialbehaviours")
            r = cursordb.fetchall()
            connectiondb.commit()
            connectiondb.close()
            startX = 25
            startY = 275
            j = 0
            for i in r:
                button = Button(self, text=i, width=15)
                button.configure(bg="#83B4B3", activebackground="#CFF6FF", command=lambda: run_file(i[0]))
                button.place(x=startX, y=startY)
                startX = startX+200
                j = j+1
                if j == 6:
                    startY = startY+100
                    startX = 25
                    j = 0

        self.configure(bg="#B6F1FF")

        scrWidth = self.winfo_screenwidth()
        scrHeight = self.winfo_screenheight()
        check_files()

        logo_small(self, parent, root)

        # Configure logout button
        logoutBut = Button(self, text="Logout", width=15, command=lambda: root.show_frame(loginPage))
        logoutBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        logoutBut.place(x=scrWidth-185, y=25)

        # Configure help button
        helpBut = Button(self, text="Help", width=15, command=lambda: root.show_frame(staffHelpPage))
        helpBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        helpBut.place(x=scrWidth-360, y=25)

        # Configure account options button
        staffAccOpBut = Button(self, text="Account Options", width=15)
        staffAccOpBut.configure(bg="#83B4B3", activebackground="#CFF6FF", command=lambda: root.show_frame(staffAccountOpPage))
        staffAccOpBut.place(x=scrWidth-535, y=25)

        # Configure button to go back to staff landing page
        backBut = Button(self, text="Back", width=15, command=lambda: [self.lower(), check_files()])
        backBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        backBut.place(x=scrWidth-185, y=scrHeight-150)

        # Configure update button
        updateBut = Button(self, text="Update", width=15, command=lambda: [update_file(), check_files()])
        updateBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        updateBut.place(x=scrWidth-360, y=scrHeight-150)

        # Configure remove button
        removeBut = Button(self, text="Remove", width=15, command=lambda: [remove_file(), check_files()])
        removeBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        removeBut.place(x=scrWidth-535, y=scrHeight-150)

        # Configure add button
        addBut = Button(self, text="Add", width=15, command=lambda: [add_file(), check_files()])
        addBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        addBut.place(x=scrWidth-710, y=scrHeight-150)

        # Configure name entry
        fileName = Entry(self, bg="#FFFFFF", width=37)
        fileName.place(x=scrWidth-1040, y=scrHeight-150, height=31)

        file_label(self, parent, root)

class staffSOandIM(tk.Frame):

    def __init__(self, parent, root):
        super().__init__(parent)

        def add_file():
            if fileName.get() == "":
                tk.messagebox.showerror("File Name Error", "Please enter the name you have chosen for the file you are uploading")
            else:
                try:
                    file = filedialog.askopenfilename(initialdir="/home/pi", title="Select file to add", filetypes=(("All Files", "*.*"), ("Python files", "*.py")))
                    fName = fileName.get()
                    with open(file, "rb") as f:
                        filedata = f.read()
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                    cursordb = connectiondb.cursor()
                    sql = "INSERT INTO securityoperationsandincidentmanagement(name, filedata) VALUES(%s, %s)"
                    cursordb.execute(sql, (fName, filedata))
                    tk.messagebox.showinfo("Success", "File successfully uploaded")
                    fileName.delete(0, 'end')
                    connectiondb.commit()
                    connectiondb.close()
                except:
                    tk.messagebox.showerror("File Error", "Error: Filename already in use")

        def remove_file():
            if fileName.get() == "":
                tk.messagebox.showerror("File Name Error", "Please enter the name of the file you wish to remove")
            else:
                try:
                    fName = fileName.get()
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                    cursordb = connectiondb.cursor()
                    cursordb.execute("DELETE FROM securityoperationsandincidentmanagement WHERE name = %s", [(fName)])
                    tk.messagebox.showinfo("Success", "File successfully deleted")
                    fileName.delete(0, 'end')
                    connectiondb.commit()
                    connectiondb.close()
                except:
                    tk.messagebox.showerror("File Error", "Error: File Not Found")

        def update_file():
            if fileName.get() == "":
                tk.messagebox.showerror("File Name Error", "Please enter the name of the file you wish to update")
            else:
                try:
                    file = filedialog.askopenfilename(initialdir="/home/pi", title="Select file to add", filetypes=(("All Files", "*.*"), ("Exe files", "*.exe"), ("PDF files", "*.pdf")))
                    fName = fileName.get()
                    with open(file, "rb") as f:
                        filedata = f.read()
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                    cursordb = connectiondb.cursor()
                    cursordb.execute("UPDATE securityoperationsandincidentmanagement SET filedata = %s WHERE name = %s", [(filedata), (fName)])
                    tk.messagebox.showinfo("Success", "File successfully updated")
                    fileName.delete(0, 'end')
                    connectiondb.commit()
                    connectiondb.close()
                except:
                    tk.messagebox.showerror("File Error", "Error: File Not Found")

        def run_file(fName):
                connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                cursordb = connectiondb.cursor()
                cursordb.execute("SELECT filedata FROM securityoperationsandincidentmanagement WHERE name = %s", [(fName)])
                r = cursordb.fetchall()
                for i in r:
                    data = i[0]
                decodedData = data.decode("utf8")
                exec(decodedData)
                connectiondb.commit()
                connectiondb.close()

        def check_files():
            connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
            cursordb = connectiondb.cursor()
            cursordb.execute("SELECT name FROM securityoperationsandincidentmanagement")
            r = cursordb.fetchall()
            connectiondb.commit()
            connectiondb.close()
            startX = 25
            startY = 275
            j = 0
            for i in r:
                button = Button(self, text=i, width=15)
                button.configure(bg="#83B4B3", activebackground="#CFF6FF", command=lambda: run_file(i[0]))
                button.place(x=startX, y=startY)
                startX = startX+200
                j = j+1
                if j == 6:
                    startY = startY+100
                    startX = 25
                    j = 0

        self.configure(bg="#B6F1FF")

        scrWidth = self.winfo_screenwidth()
        scrHeight = self.winfo_screenheight()
        check_files()

        logo_small(self, parent, root)

        # Configure logout button
        logoutBut = Button(self, text="Logout", width=15, command=lambda: root.show_frame(loginPage))
        logoutBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        logoutBut.place(x=scrWidth-185, y=25)

        # Configure help button
        helpBut = Button(self, text="Help", width=15, command=lambda: root.show_frame(staffHelpPage))
        helpBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        helpBut.place(x=scrWidth-360, y=25)

        # Configure account options button
        staffAccOpBut = Button(self, text="Account Options", width=15)
        staffAccOpBut.configure(bg="#83B4B3", activebackground="#CFF6FF", command=lambda: root.show_frame(staffAccountOpPage))
        staffAccOpBut.place(x=scrWidth-535, y=25)

        # Configure button to go back to staff landing page
        backBut = Button(self, text="Back", width=15, command=lambda: [self.lower(), check_files()])
        backBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        backBut.place(x=scrWidth-185, y=scrHeight-150)

        # Configure update button
        updateBut = Button(self, text="Update", width=15, command=lambda: [update_file(), check_files()])
        updateBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        updateBut.place(x=scrWidth-360, y=scrHeight-150)

        # Configure remove button
        removeBut = Button(self, text="Remove", width=15, command=lambda: [remove_file(), check_files()])
        removeBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        removeBut.place(x=scrWidth-535, y=scrHeight-150)

        # Configure add button
        addBut = Button(self, text="Add", width=15, command=lambda: [add_file(), check_files()])
        addBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        addBut.place(x=scrWidth-710, y=scrHeight-150)

        # Configure name entry
        fileName = Entry(self, bg="#FFFFFF", width=37)
        fileName.place(x=scrWidth-1040, y=scrHeight-150, height=31)

        file_label(self, parent, root)

class staffForensics(tk.Frame):

    def __init__(self, parent, root):
        super().__init__(parent)

        def add_file():
            if fileName.get() == "":
                tk.messagebox.showerror("File Name Error", "Please enter the name you have chosen for the file you are uploading")
            else:
                try:
                    file = filedialog.askopenfilename(initialdir="/home/pi", title="Select file to add", filetypes=(("All Files", "*.*"), ("Python files", "*.py")))
                    fName = fileName.get()
                    with open(file, "rb") as f:
                        filedata = f.read()
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                    cursordb = connectiondb.cursor()
                    sql = "INSERT INTO forensics(name, filedata) VALUES(%s, %s)"
                    cursordb.execute(sql, (fName, filedata))
                    tk.messagebox.showinfo("Success", "File successfully uploaded")
                    fileName.delete(0, 'end')
                    connectiondb.commit()
                    connectiondb.close()
                except:
                    tk.messagebox.showerror("File Error", "Error: Filename not found")

        def remove_file():
            if fileName.get() == "":
                tk.messagebox.showerror("File Name Error", "Please enter the name of the file you wish to remove")
            else:
                try:
                    fName = fileName.get()
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                    cursordb = connectiondb.cursor()
                    cursordb.execute("DELETE FROM forensics WHERE name = %s", [(fName)])
                    tk.messagebox.showinfo("Success", "File successfully deleted")
                    fileName.delete(0, 'end')
                    connectiondb.commit()
                    connectiondb.close()
                except:
                    tk.messagebox.showerror("File Error", "Error: File Not Found")

        def update_file():
            if fileName.get() == "":
                tk.messagebox.showerror("File Name Error", "Please enter the name of the file you wish to update")
            else:
                try:
                    file = filedialog.askopenfilename(initialdir="/home/pi", title="Select file to add", filetypes=(("All Files", "*.*"), ("Exe files", "*.exe"), ("PDF files", "*.pdf")))
                    fName = fileName.get()
                    with open(file, "rb") as f:
                        filedata = f.read()
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                    cursordb = connectiondb.cursor()
                    cursordb.execute("UPDATE forensics SET filedata = %s WHERE name = %s", [(filedata), (fName)])
                    tk.messagebox.showinfo("Success", "File successfully updated")
                    fileName.delete(0, 'end')
                    connectiondb.commit()
                    connectiondb.close()
                except:
                    tk.messagebox.showerror("File Error", "Error: File Not Found")

        def run_file(fName):
                connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                cursordb = connectiondb.cursor()
                cursordb.execute("SELECT filedata FROM forensics WHERE name = %s", [(fName)])
                r = cursordb.fetchall()
                for i in r:
                    data = i[0]
                decodedData = data.decode("utf8")
                exec(decodedData)
                connectiondb.commit()
                connectiondb.close()

        def check_files():
            connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
            cursordb = connectiondb.cursor()
            cursordb.execute("SELECT name FROM forensics")
            r = cursordb.fetchall()
            connectiondb.commit()
            connectiondb.close()
            startX = 25
            startY = 275
            j = 0
            for i in r:
                button = Button(self, text=i, width=15)
                button.configure(bg="#83B4B3", activebackground="#CFF6FF", command=lambda: run_file(i[0]))
                button.place(x=startX, y=startY)
                startX = startX+200
                j = j+1
                if j == 6:
                    startY = startY+100
                    startX = 25
                    j = 0

        self.configure(bg="#B6F1FF")

        scrWidth = self.winfo_screenwidth()
        scrHeight = self.winfo_screenheight()
        check_files()

        logo_small(self, parent, root)

        # Configure logout button
        logoutBut = Button(self, text="Logout", width=15, command=lambda: root.show_frame(loginPage))
        logoutBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        logoutBut.place(x=scrWidth-185, y=25)

        # Configure help button
        helpBut = Button(self, text="Help", width=15, command=lambda: root.show_frame(staffHelpPage))
        helpBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        helpBut.place(x=scrWidth-360, y=25)

        # Configure account options button
        staffAccOpBut = Button(self, text="Account Options", width=15)
        staffAccOpBut.configure(bg="#83B4B3", activebackground="#CFF6FF", command=lambda: root.show_frame(staffAccountOpPage))
        staffAccOpBut.place(x=scrWidth-535, y=25)

        # Configure button to go back to staff landing page
        backBut = Button(self, text="Back", width=15, command=lambda: [self.lower(), check_files()])
        backBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        backBut.place(x=scrWidth-185, y=scrHeight-150)

        # Configure update button
        updateBut = Button(self, text="Update", width=15, command=lambda: [update_file(), check_files()])
        updateBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        updateBut.place(x=scrWidth-360, y=scrHeight-150)

        # Configure remove button
        removeBut = Button(self, text="Remove", width=15, command=lambda: [remove_file(), check_files()])
        removeBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        removeBut.place(x=scrWidth-535, y=scrHeight-150)

        # Configure add button
        addBut = Button(self, text="Add", width=15, command=lambda: [add_file(), check_files()])
        addBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        addBut.place(x=scrWidth-710, y=scrHeight-150)

        # Configure name entry
        fileName = Entry(self, bg="#FFFFFF", width=37)
        fileName.place(x=scrWidth-1040, y=scrHeight-150, height=31)

        file_label(self, parent, root)

class staffCryptography(tk.Frame):

    def __init__(self, parent, root):
        super().__init__(parent)

        def add_file():
            if fileName.get() == "":
                tk.messagebox.showerror("File Name Error", "Please enter the name you have chosen for the file you are uploading")
            else:
                try:
                    file = filedialog.askopenfilename(initialdir="/home/pi", title="Select file to add", filetypes=(("All Files", "*.*"), ("Python files", "*.py")))
                    fName = fileName.get()
                    with open(file, "rb") as f:
                        filedata = f.read()
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                    cursordb = connectiondb.cursor()
                    sql = "INSERT INTO cryptography(name, filedata) VALUES(%s, %s)"
                    cursordb.execute(sql, (fName, filedata))
                    tk.messagebox.showinfo("Success", "File successfully uploaded")
                    fileName.delete(0, 'end')
                    connectiondb.commit()
                    connectiondb.close()
                except:
                    tk.messagebox.showerror("File Error", "Error: Filename not found")

        def remove_file():
            if fileName.get() == "":
                tk.messagebox.showerror("File Name Error", "Please enter the name of the file you wish to remove")
            else:
                try:
                    fName = fileName.get()
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                    cursordb = connectiondb.cursor()
                    cursordb.execute("DELETE FROM cryptography WHERE name = %s", [(fName)])
                    tk.messagebox.showinfo("Success", "File successfully deleted")
                    fileName.delete(0, 'end')
                    connectiondb.commit()
                    connectiondb.close()
                except:
                    tk.messagebox.showerror("File Error", "Error: File Not Found")

        def update_file():
            if fileName.get() == "":
                tk.messagebox.showerror("File Name Error", "Please enter the name of the file you wish to update")
            else:
                try:
                    file = filedialog.askopenfilename(initialdir="/home/pi", title="Select file to add", filetypes=(("All Files", "*.*"), ("Exe files", "*.exe"), ("PDF files", "*.pdf")))
                    fName = fileName.get()
                    with open(file, "rb") as f:
                        filedata = f.read()
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                    cursordb = connectiondb.cursor()
                    cursordb.execute("UPDATE cryptography SET filedata = %s WHERE name = %s", [(filedata), (fName)])
                    tk.messagebox.showinfo("Success", "File successfully updated")
                    fileName.delete(0, 'end')
                    connectiondb.commit()
                    connectiondb.close()
                except:
                    tk.messagebox.showerror("File Error", "Error: File Not Found")

        def run_file(fName):
                connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                cursordb = connectiondb.cursor()
                cursordb.execute("SELECT filedata FROM cryptography WHERE name = %s", [(fName)])
                r = cursordb.fetchall()
                for i in r:
                    data = i[0]
                decodedData = data.decode("utf8")
                exec(decodedData)
                connectiondb.commit()
                connectiondb.close()

        def check_files():
            connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
            cursordb = connectiondb.cursor()
            cursordb.execute("SELECT name FROM cryptography")
            r = cursordb.fetchall()
            connectiondb.commit()
            connectiondb.close()
            startX = 25
            startY = 275
            j = 0
            for i in r:
                button = Button(self, text=i, width=15)
                button.configure(bg="#83B4B3", activebackground="#CFF6FF", command=lambda: run_file(i[0]))
                button.place(x=startX, y=startY)
                startX = startX+200
                j = j+1
                if j == 6:
                    startY = startY+100
                    startX = 25
                    j = 0

        self.configure(bg="#B6F1FF")

        scrWidth = self.winfo_screenwidth()
        scrHeight = self.winfo_screenheight()
        check_files()

        logo_small(self, parent, root)

        # Configure logout button
        logoutBut = Button(self, text="Logout", width=15, command=lambda: root.show_frame(loginPage))
        logoutBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        logoutBut.place(x=scrWidth-185, y=25)

        # Configure help button
        helpBut = Button(self, text="Help", width=15, command=lambda: root.show_frame(staffHelpPage))
        helpBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        helpBut.place(x=scrWidth-360, y=25)

        # Configure account options button
        staffAccOpBut = Button(self, text="Account Options", width=15)
        staffAccOpBut.configure(bg="#83B4B3", activebackground="#CFF6FF", command=lambda: root.show_frame(staffAccountOpPage))
        staffAccOpBut.place(x=scrWidth-535, y=25)

        # Configure button to go back to staff landing page
        backBut = Button(self, text="Back", width=15, command=lambda: [self.lower(), check_files()])
        backBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        backBut.place(x=scrWidth-185, y=scrHeight-150)

        # Configure update button
        updateBut = Button(self, text="Update", width=15, command=lambda: [update_file(), check_files()])
        updateBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        updateBut.place(x=scrWidth-360, y=scrHeight-150)

        # Configure remove button
        removeBut = Button(self, text="Remove", width=15, command=lambda: [remove_file(), check_files()])
        removeBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        removeBut.place(x=scrWidth-535, y=scrHeight-150)

        # Configure add button
        addBut = Button(self, text="Add", width=15, command=lambda: [add_file(), check_files()])
        addBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        addBut.place(x=scrWidth-710, y=scrHeight-150)

        # Configure name entry
        fileName = Entry(self, bg="#FFFFFF", width=37)
        fileName.place(x=scrWidth-1040, y=scrHeight-150, height=31)

        file_label(self, parent, root)

class staffOSandVS(tk.Frame):

    def __init__(self, parent, root):
        super().__init__(parent)

        def add_file():
            if fileName.get() == "":
                tk.messagebox.showerror("File Name Error", "Please enter the name you have chosen for the file you are uploading")
            else:
                try:
                    file = filedialog.askopenfilename(initialdir="/home/pi", title="Select file to add", filetypes=(("All Files", "*.*"), ("Python files", "*.py")))
                    fName = fileName.get()
                    with open(file, "rb") as f:
                        filedata = f.read()
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                    cursordb = connectiondb.cursor()
                    sql = "INSERT INTO operatingsystemsandvirtualisationsecurity(name, filedata) VALUES(%s, %s)"
                    cursordb.execute(sql, (fName, filedata))
                    tk.messagebox.showinfo("Success", "File successfully uploaded")
                    fileName.delete(0, 'end')
                    connectiondb.commit()
                    connectiondb.close()
                except:
                    tk.messagebox.showerror("File Error", "Error: Filename not found")

        def remove_file():
            if fileName.get() == "":
                tk.messagebox.showerror("File Name Error", "Please enter the name of the file you wish to remove")
            else:
                try:
                    fName = fileName.get()
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                    cursordb = connectiondb.cursor()
                    cursordb.execute("DELETE FROM operatingsystemsandvirtualisationsecurity WHERE name = %s", [(fName)])
                    tk.messagebox.showinfo("Success", "File successfully deleted")
                    fileName.delete(0, 'end')
                    connectiondb.commit()
                    connectiondb.close()
                except:
                    tk.messagebox.showerror("File Error", "Error: File Not Found")

        def update_file():
            if fileName.get() == "":
                tk.messagebox.showerror("File Name Error", "Please enter the name of the file you wish to update")
            else:
                try:
                    file = filedialog.askopenfilename(initialdir="/home/pi", title="Select file to add", filetypes=(("All Files", "*.*"), ("Exe files", "*.exe"), ("PDF files", "*.pdf")))
                    fName = fileName.get()
                    with open(file, "rb") as f:
                        filedata = f.read()
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                    cursordb = connectiondb.cursor()
                    cursordb.execute("UPDATE operatingsystemsandvirtualisationsecurity SET filedata = %s WHERE name = %s", [(filedata), (fName)])
                    tk.messagebox.showinfo("Success", "File successfully updated")
                    fileName.delete(0, 'end')
                    connectiondb.commit()
                    connectiondb.close()
                except:
                    tk.messagebox.showerror("File Error", "Error: File Not Found")

        def run_file(fName):
                connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                cursordb = connectiondb.cursor()
                cursordb.execute("SELECT filedata FROM operatingsystemsandvirtualisationsecurity WHERE name = %s", [(fName)])
                r = cursordb.fetchall()
                for i in r:
                    data = i[0]
                decodedData = data.decode("utf8")
                exec(decodedData)
                connectiondb.commit()
                connectiondb.close()

        def check_files():
            connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
            cursordb = connectiondb.cursor()
            cursordb.execute("SELECT name FROM operatingsystemsandvirtualisationsecurity")
            r = cursordb.fetchall()
            connectiondb.commit()
            connectiondb.close()
            startX = 25
            startY = 275
            j = 0
            for i in r:
                button = Button(self, text=i, width=15)
                button.configure(bg="#83B4B3", activebackground="#CFF6FF", command=lambda: run_file(i[0]))
                button.place(x=startX, y=startY)
                startX = startX+200
                j = j+1
                if j == 6:
                    startY = startY+100
                    startX = 25
                    j = 0

        self.configure(bg="#B6F1FF")

        scrWidth = self.winfo_screenwidth()
        scrHeight = self.winfo_screenheight()
        check_files()

        logo_small(self, parent, root)

        # Configure logout button
        logoutBut = Button(self, text="Logout", width=15, command=lambda: root.show_frame(loginPage))
        logoutBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        logoutBut.place(x=scrWidth-185, y=25)

        # Configure help button
        helpBut = Button(self, text="Help", width=15, command=lambda: root.show_frame(staffHelpPage))
        helpBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        helpBut.place(x=scrWidth-360, y=25)

        # Configure account options button
        staffAccOpBut = Button(self, text="Account Options", width=15)
        staffAccOpBut.configure(bg="#83B4B3", activebackground="#CFF6FF", command=lambda: root.show_frame(staffAccountOpPage))
        staffAccOpBut.place(x=scrWidth-535, y=25)

        # Configure button to go back to staff landing page
        backBut = Button(self, text="Back", width=15, command=lambda: [self.lower(), check_files()])
        backBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        backBut.place(x=scrWidth-185, y=scrHeight-150)

        # Configure update button
        updateBut = Button(self, text="Update", width=15, command=lambda: [update_file(), check_files()])
        updateBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        updateBut.place(x=scrWidth-360, y=scrHeight-150)

        # Configure remove button
        removeBut = Button(self, text="Remove", width=15, command=lambda: [remove_file(), check_files()])
        removeBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        removeBut.place(x=scrWidth-535, y=scrHeight-150)

        # Configure add button
        addBut = Button(self, text="Add", width=15, command=lambda: [add_file(), check_files()])
        addBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        addBut.place(x=scrWidth-710, y=scrHeight-150)

        # Configure name entry
        fileName = Entry(self, bg="#FFFFFF", width=37)
        fileName.place(x=scrWidth-1040, y=scrHeight-150, height=31)

        file_label(self, parent, root)

class staffDSS(tk.Frame):

    def __init__(self, parent, root):
        super().__init__(parent)

        def add_file():
            if fileName.get() == "":
                tk.messagebox.showerror("File Name Error", "Please enter the name you have chosen for the file you are uploading")
            else:
                try:
                    file = filedialog.askopenfilename(initialdir="/home/pi", title="Select file to add", filetypes=(("All Files", "*.*"), ("Python files", "*.py")))
                    fName = fileName.get()
                    with open(file, "rb") as f:
                        filedata = f.read()
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                    cursordb = connectiondb.cursor()
                    sql = "INSERT INTO distributedsystemssecurity(name, filedata) VALUES(%s, %s)"
                    cursordb.execute(sql, (fName, filedata))
                    tk.messagebox.showinfo("Success", "File successfully uploaded")
                    fileName.delete(0, 'end')
                    connectiondb.commit()
                    connectiondb.close()
                except:
                    tk.messagebox.showerror("File Error", "Error: Filename already in use")

        def remove_file():
            if fileName.get() == "":
                tk.messagebox.showerror("File Name Error", "Please enter the name of the file you wish to remove")
            else:
                try:
                    fName = fileName.get()
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                    cursordb = connectiondb.cursor()
                    cursordb.execute("DELETE FROM distributedsystemssecurity WHERE name = %s", [(fName)])
                    tk.messagebox.showinfo("Success", "File successfully deleted")
                    fileName.delete(0, 'end')
                    connectiondb.commit()
                    connectiondb.close()
                except:
                    tk.messagebox.showerror("File Error", "Error: File Not Found")

        def update_file():
            if fileName.get() == "":
                tk.messagebox.showerror("File Name Error", "Please enter the name of the file you wish to update")
            else:
                try:
                    file = filedialog.askopenfilename(initialdir="/home/pi", title="Select file to add", filetypes=(("All Files", "*.*"), ("Exe files", "*.exe"), ("PDF files", "*.pdf")))
                    fName = fileName.get()
                    with open(file, "rb") as f:
                        filedata = f.read()
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                    cursordb = connectiondb.cursor()
                    cursordb.execute("UPDATE distributedsystemssecurity SET filedata = %s WHERE name = %s", [(filedata), (fName)])
                    tk.messagebox.showinfo("Success", "File successfully updated")
                    fileName.delete(0, 'end')
                    connectiondb.commit()
                    connectiondb.close()
                except:
                    tk.messagebox.showerror("File Error", "Error: File Not Found")

        def run_file(fName):
                connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                cursordb = connectiondb.cursor()
                cursordb.execute("SELECT filedata FROM distributedsystemssecurity WHERE name = %s", [(fName)])
                r = cursordb.fetchall()
                for i in r:
                    data = i[0]
                decodedData = data.decode("utf8")
                exec(decodedData)
                connectiondb.commit()
                connectiondb.close()

        def check_files():
            connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
            cursordb = connectiondb.cursor()
            cursordb.execute("SELECT name FROM distributedsystemssecurity")
            r = cursordb.fetchall()
            connectiondb.commit()
            connectiondb.close()
            startX = 25
            startY = 275
            j = 0
            for i in r:
                button = Button(self, text=i, width=15)
                button.configure(bg="#83B4B3", activebackground="#CFF6FF", command=lambda: run_file(i[0]))
                button.place(x=startX, y=startY)
                startX = startX+200
                j = j+1
                if j == 6:
                    startY = startY+100
                    startX = 25
                    j = 0

        self.configure(bg="#B6F1FF")

        scrWidth = self.winfo_screenwidth()
        scrHeight = self.winfo_screenheight()
        check_files()

        logo_small(self, parent, root)

        # Configure logout button
        logoutBut = Button(self, text="Logout", width=15, command=lambda: root.show_frame(loginPage))
        logoutBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        logoutBut.place(x=scrWidth-185, y=25)

        # Configure help button
        helpBut = Button(self, text="Help", width=15, command=lambda: root.show_frame(staffHelpPage))
        helpBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        helpBut.place(x=scrWidth-360, y=25)

        # Configure account options button
        staffAccOpBut = Button(self, text="Account Options", width=15)
        staffAccOpBut.configure(bg="#83B4B3", activebackground="#CFF6FF", command=lambda: root.show_frame(staffAccountOpPage))
        staffAccOpBut.place(x=scrWidth-535, y=25)

        # Configure button to go back to staff landing page
        backBut = Button(self, text="Back", width=15, command=lambda: [self.lower(), check_files()])
        backBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        backBut.place(x=scrWidth-185, y=scrHeight-150)

        # Configure update button
        updateBut = Button(self, text="Update", width=15, command=lambda: [update_file(), check_files()])
        updateBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        updateBut.place(x=scrWidth-360, y=scrHeight-150)

        # Configure remove button
        removeBut = Button(self, text="Remove", width=15, command=lambda: [remove_file(), check_files()])
        removeBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        removeBut.place(x=scrWidth-535, y=scrHeight-150)

        # Configure add button
        addBut = Button(self, text="Add", width=15, command=lambda: [add_file(), check_files()])
        addBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        addBut.place(x=scrWidth-710, y=scrHeight-150)

        # Configure name entry
        fileName = Entry(self, bg="#FFFFFF", width=37)
        fileName.place(x=scrWidth-1040, y=scrHeight-150, height=31)

        file_label(self, parent, root)

class staffAAandA(tk.Frame):

    def __init__(self, parent, root):
        super().__init__(parent)

        def add_file():
            if fileName.get() == "":
                tk.messagebox.showerror("File Name Error", "Please enter the name you have chosen for the file you are uploading")
            else:
                try:
                    file = filedialog.askopenfilename(initialdir="/home/pi", title="Select file to add", filetypes=(("All Files", "*.*"), ("Python files", "*.py")))
                    fName = fileName.get()
                    with open(file, "rb") as f:
                        filedata = f.read()
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                    cursordb = connectiondb.cursor()
                    sql = "INSERT INTO authenticationauthorisationandaccountability(name, filedata) VALUES(%s, %s)"
                    cursordb.execute(sql, (fName, filedata))
                    tk.messagebox.showinfo("Success", "File successfully uploaded")
                    fileName.delete(0, 'end')
                    connectiondb.commit()
                    connectiondb.close()
                except:
                    tk.messagebox.showerror("File Error", "Error: Filename already in use")

        def remove_file():
            if fileName.get() == "":
                tk.messagebox.showerror("File Name Error", "Please enter the name of the file you wish to remove")
            else:
                try:
                    fName = fileName.get()
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                    cursordb = connectiondb.cursor()
                    cursordb.execute("DELETE FROM authenticationauthorisationandaccountability WHERE name = %s", [(fName)])
                    tk.messagebox.showinfo("Success", "File successfully deleted")
                    fileName.delete(0, 'end')
                    connectiondb.commit()
                    connectiondb.close()
                except:
                    tk.messagebox.showerror("File Error", "Error: File Not Found")

        def update_file():
            if fileName.get() == "":
                tk.messagebox.showerror("File Name Error", "Please enter the name of the file you wish to update")
            else:
                try:
                    file = filedialog.askopenfilename(initialdir="/home/pi", title="Select file to add", filetypes=(("All Files", "*.*"), ("Exe files", "*.exe"), ("PDF files", "*.pdf")))
                    fName = fileName.get()
                    with open(file, "rb") as f:
                        filedata = f.read()
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                    cursordb = connectiondb.cursor()
                    cursordb.execute("UPDATE authenticationauthorisationandaccountability SET filedata = %s WHERE name = %s", [(filedata), (fName)])
                    tk.messagebox.showinfo("Success", "File successfully updated")
                    fileName.delete(0, 'end')
                    connectiondb.commit()
                    connectiondb.close()
                except:
                    tk.messagebox.showerror("File Error", "Error: File Not Found")

        def run_file(fName):
                connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                cursordb = connectiondb.cursor()
                cursordb.execute("SELECT filedata FROM authenticationauthorisationandaccountability WHERE name = %s", [(fName)])
                r = cursordb.fetchall()
                for i in r:
                    data = i[0]
                decodedData = data.decode("utf8")
                exec(decodedData)
                connectiondb.commit()
                connectiondb.close()

        def check_files():
            connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
            cursordb = connectiondb.cursor()
            cursordb.execute("SELECT name FROM authenticationauthorisationandaccountability")
            r = cursordb.fetchall()
            connectiondb.commit()
            connectiondb.close()
            startX = 25
            startY = 275
            j = 0
            for i in r:
                button = Button(self, text=i, width=15)
                button.configure(bg="#83B4B3", activebackground="#CFF6FF", command=lambda: run_file(i[0]))
                button.place(x=startX, y=startY)
                startX = startX+200
                j = j+1
                if j == 6:
                    startY = startY+100
                    startX = 25
                    j = 0

        self.configure(bg="#B6F1FF")

        scrWidth = self.winfo_screenwidth()
        scrHeight = self.winfo_screenheight()
        check_files()

        logo_small(self, parent, root)

        # Configure logout button
        logoutBut = Button(self, text="Logout", width=15, command=lambda: root.show_frame(loginPage))
        logoutBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        logoutBut.place(x=scrWidth-185, y=25)

        # Configure help button
        helpBut = Button(self, text="Help", width=15, command=lambda: root.show_frame(staffHelpPage))
        helpBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        helpBut.place(x=scrWidth-360, y=25)

        # Configure account options button
        staffAccOpBut = Button(self, text="Account Options", width=15)
        staffAccOpBut.configure(bg="#83B4B3", activebackground="#CFF6FF", command=lambda: root.show_frame(staffAccountOpPage))
        staffAccOpBut.place(x=scrWidth-535, y=25)

        # Configure button to go back to staff landing page
        backBut = Button(self, text="Back", width=15, command=lambda: [self.lower(), check_files()])
        backBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        backBut.place(x=scrWidth-185, y=scrHeight-150)

        # Configure update button
        updateBut = Button(self, text="Update", width=15, command=lambda: [update_file(), check_files()])
        updateBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        updateBut.place(x=scrWidth-360, y=scrHeight-150)

        # Configure remove button
        removeBut = Button(self, text="Remove", width=15, command=lambda: [remove_file(), check_files()])
        removeBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        removeBut.place(x=scrWidth-535, y=scrHeight-150)

        # Configure add button
        addBut = Button(self, text="Add", width=15, command=lambda: [add_file(), check_files()])
        addBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        addBut.place(x=scrWidth-710, y=scrHeight-150)

        # Configure name entry
        fileName = Entry(self, bg="#FFFFFF", width=37)
        fileName.place(x=scrWidth-1040, y=scrHeight-150, height=31)

        file_label(self, parent, root)

class staffSoftwareSecurity(tk.Frame):

    def __init__(self, parent, root):
        super().__init__(parent)

        def add_file():
            if fileName.get() == "":
                tk.messagebox.showerror("File Name Error", "Please enter the name you have chosen for the file you are uploading")
            else:
                try:
                    file = filedialog.askopenfilename(initialdir="/home/pi", title="Select file to add", filetypes=(("All Files", "*.*"), ("Python files", "*.py")))
                    fName = fileName.get()
                    with open(file, "rb") as f:
                        filedata = f.read()
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                    cursordb = connectiondb.cursor()
                    sql = "INSERT INTO softwaresecurity(name, filedata) VALUES(%s, %s)"
                    cursordb.execute(sql, (fName, filedata))
                    tk.messagebox.showinfo("Success", "File successfully uploaded")
                    fileName.delete(0, 'end')
                    connectiondb.commit()
                    connectiondb.close()
                except:
                    tk.messagebox.showerror("File Error", "Error: Filename already in use")

        def remove_file():
            if fileName.get() == "":
                tk.messagebox.showerror("File Name Error", "Please enter the name of the file you wish to remove")
            else:
                try:
                    fName = fileName.get()
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                    cursordb = connectiondb.cursor()
                    cursordb.execute("DELETE FROM softwaresecurity WHERE name = %s", [(fName)])
                    tk.messagebox.showinfo("Success", "File successfully deleted")
                    fileName.delete(0, 'end')
                    connectiondb.commit()
                    connectiondb.close()
                except:
                    tk.messagebox.showerror("File Error", "Error: File Not Found")

        def update_file():
            if fileName.get() == "":
                tk.messagebox.showerror("File Name Error", "Please enter the name of the file you wish to update")
            else:
                try:
                    file = filedialog.askopenfilename(initialdir="/home/pi", title="Select file to add", filetypes=(("All Files", "*.*"), ("Exe files", "*.exe"), ("PDF files", "*.pdf")))
                    fName = fileName.get()
                    with open(file, "rb") as f:
                        filedata = f.read()
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                    cursordb = connectiondb.cursor()
                    cursordb.execute("UPDATE softwaresecurity SET filedata = %s WHERE name = %s", [(filedata), (fName)])
                    tk.messagebox.showinfo("Success", "File successfully updated")
                    fileName.delete(0, 'end')
                    connectiondb.commit()
                    connectiondb.close()
                except:
                    tk.messagebox.showerror("File Error", "Error: File Not Found")

        def run_file(fName):
                connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                cursordb = connectiondb.cursor()
                cursordb.execute("SELECT filedata FROM softwaresecurity WHERE name = %s", [(fName)])
                r = cursordb.fetchall()
                for i in r:
                    data = i[0]
                decodedData = data.decode("utf8")
                exec(decodedData)
                connectiondb.commit()
                connectiondb.close()

        def check_files():
            connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
            cursordb = connectiondb.cursor()
            cursordb.execute("SELECT name FROM softwaresecurity")
            r = cursordb.fetchall()
            connectiondb.commit()
            connectiondb.close()
            startX = 25
            startY = 275
            j = 0
            for i in r:
                button = Button(self, text=i, width=15)
                button.configure(bg="#83B4B3", activebackground="#CFF6FF", command=lambda: run_file(i[0]))
                button.place(x=startX, y=startY)
                startX = startX+200
                j = j+1
                if j == 6:
                    startY = startY+100
                    startX = 25
                    j = 0

        self.configure(bg="#B6F1FF")

        scrWidth = self.winfo_screenwidth()
        scrHeight = self.winfo_screenheight()
        check_files()

        logo_small(self, parent, root)

        # Configure logout button
        logoutBut = Button(self, text="Logout", width=15, command=lambda: root.show_frame(loginPage))
        logoutBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        logoutBut.place(x=scrWidth-185, y=25)

        # Configure help button
        helpBut = Button(self, text="Help", width=15, command=lambda: root.show_frame(staffHelpPage))
        helpBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        helpBut.place(x=scrWidth-360, y=25)

        # Configure account options button
        staffAccOpBut = Button(self, text="Account Options", width=15)
        staffAccOpBut.configure(bg="#83B4B3", activebackground="#CFF6FF", command=lambda: root.show_frame(staffAccountOpPage))
        staffAccOpBut.place(x=scrWidth-535, y=25)

        # Configure button to go back to staff landing page
        backBut = Button(self, text="Back", width=15, command=lambda: [self.lower(), check_files()])
        backBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        backBut.place(x=scrWidth-185, y=scrHeight-150)

        # Configure update button
        updateBut = Button(self, text="Update", width=15, command=lambda: [update_file(), check_files()])
        updateBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        updateBut.place(x=scrWidth-360, y=scrHeight-150)

        # Configure remove button
        removeBut = Button(self, text="Remove", width=15, command=lambda: [remove_file(), check_files()])
        removeBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        removeBut.place(x=scrWidth-535, y=scrHeight-150)

        # Configure add button
        addBut = Button(self, text="Add", width=15, command=lambda: [add_file(), check_files()])
        addBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        addBut.place(x=scrWidth-710, y=scrHeight-150)

        # Configure name entry
        fileName = Entry(self, bg="#FFFFFF", width=37)
        fileName.place(x=scrWidth-1040, y=scrHeight-150, height=31)

        file_label(self, parent, root)

class staffWandMS(tk.Frame):

    def __init__(self, parent, root):
        super().__init__(parent)

        def add_file():
            if fileName.get() == "":
                tk.messagebox.showerror("File Name Error", "Please enter the name you have chosen for the file you are uploading")
            else:
                try:
                    file = filedialog.askopenfilename(initialdir="/home/pi", title="Select file to add", filetypes=(("All Files", "*.*"), ("Python files", "*.py")))
                    fName = fileName.get()
                    with open(file, "rb") as f:
                        filedata = f.read()
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                    cursordb = connectiondb.cursor()
                    sql = "INSERT INTO webandmobilesecurity(name, filedata) VALUES(%s, %s)"
                    cursordb.execute(sql, (fName, filedata))
                    tk.messagebox.showinfo("Success", "File successfully uploaded")
                    fileName.delete(0, 'end')
                    connectiondb.commit()
                    connectiondb.close()
                except:
                    tk.messagebox.showerror("File Error", "Error: Filename already in use")

        def remove_file():
            if fileName.get() == "":
                tk.messagebox.showerror("File Name Error", "Please enter the name of the file you wish to remove")
            else:
                try:
                    fName = fileName.get()
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                    cursordb = connectiondb.cursor()
                    cursordb.execute("DELETE FROM webandmobilesecurity WHERE name = %s", [(fName)])
                    tk.messagebox.showinfo("Success", "File successfully deleted")
                    fileName.delete(0, 'end')
                    connectiondb.commit()
                    connectiondb.close()
                except:
                    tk.messagebox.showerror("File Error", "Error: File Not Found")

        def update_file():
            if fileName.get() == "":
                tk.messagebox.showerror("File Name Error", "Please enter the name of the file you wish to update")
            else:
                try:
                    file = filedialog.askopenfilename(initialdir="/home/pi", title="Select file to add", filetypes=(("All Files", "*.*"), ("Exe files", "*.exe"), ("PDF files", "*.pdf")))
                    fName = fileName.get()
                    with open(file, "rb") as f:
                        filedata = f.read()
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                    cursordb = connectiondb.cursor()
                    cursordb.execute("UPDATE webandmobilesecurity SET filedata = %s WHERE name = %s", [(filedata), (fName)])
                    tk.messagebox.showinfo("Success", "File successfully updated")
                    fileName.delete(0, 'end')
                    connectiondb.commit()
                    connectiondb.close()
                except:
                    tk.messagebox.showerror("File Error", "Error: File Not Found")

        def run_file(fName):
                connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                cursordb = connectiondb.cursor()
                cursordb.execute("SELECT filedata FROM webandmobilesecurity WHERE name = %s", [(fName)])
                r = cursordb.fetchall()
                for i in r:
                    data = i[0]
                decodedData = data.decode("utf8")
                exec(decodedData)
                connectiondb.commit()
                connectiondb.close()

        def check_files():
            connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
            cursordb = connectiondb.cursor()
            cursordb.execute("SELECT name FROM webandmobilesecurity")
            r = cursordb.fetchall()
            connectiondb.commit()
            connectiondb.close()
            startX = 25
            startY = 275
            j = 0
            for i in r:
                button = Button(self, text=i, width=15)
                button.configure(bg="#83B4B3", activebackground="#CFF6FF", command=lambda: run_file(i[0]))
                button.place(x=startX, y=startY)
                startX = startX+200
                j = j+1
                if j == 6:
                    startY = startY+100
                    startX = 25
                    j = 0

        self.configure(bg="#B6F1FF")

        scrWidth = self.winfo_screenwidth()
        scrHeight = self.winfo_screenheight()
        check_files()

        logo_small(self, parent, root)

        # Configure logout button
        logoutBut = Button(self, text="Logout", width=15, command=lambda: root.show_frame(loginPage))
        logoutBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        logoutBut.place(x=scrWidth-185, y=25)

        # Configure help button
        helpBut = Button(self, text="Help", width=15, command=lambda: root.show_frame(staffHelpPage))
        helpBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        helpBut.place(x=scrWidth-360, y=25)

        # Configure account options button
        staffAccOpBut = Button(self, text="Account Options", width=15)
        staffAccOpBut.configure(bg="#83B4B3", activebackground="#CFF6FF", command=lambda: root.show_frame(staffAccountOpPage))
        staffAccOpBut.place(x=scrWidth-535, y=25)

        # Configure button to go back to staff landing page
        backBut = Button(self, text="Back", width=15, command=lambda: [self.lower(), check_files()])
        backBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        backBut.place(x=scrWidth-185, y=scrHeight-150)

        # Configure update button
        updateBut = Button(self, text="Update", width=15, command=lambda: [update_file(), check_files()])
        updateBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        updateBut.place(x=scrWidth-360, y=scrHeight-150)

        # Configure remove button
        removeBut = Button(self, text="Remove", width=15, command=lambda: [remove_file(), check_files()])
        removeBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        removeBut.place(x=scrWidth-535, y=scrHeight-150)

        # Configure add button
        addBut = Button(self, text="Add", width=15, command=lambda: [add_file(), check_files()])
        addBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        addBut.place(x=scrWidth-710, y=scrHeight-150)

        # Configure name entry
        fileName = Entry(self, bg="#FFFFFF", width=37)
        fileName.place(x=scrWidth-1040, y=scrHeight-150, height=31)

        file_label(self, parent, root)

class staffSSL(tk.Frame):

    def __init__(self, parent, root):
        super().__init__(parent)

        def add_file():
            if fileName.get() == "":
                tk.messagebox.showerror("File Name Error", "Please enter the name you have chosen for the file you are uploading")
            else:
                try:
                    file = filedialog.askopenfilename(initialdir="/home/pi", title="Select file to add", filetypes=(("All Files", "*.*"), ("Python files", "*.py")))
                    fName = fileName.get()
                    with open(file, "rb") as f:
                        filedata = f.read()
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                    cursordb = connectiondb.cursor()
                    sql = "INSERT INTO securesoftwarelifecycle(name, filedata) VALUES(%s, %s)"
                    cursordb.execute(sql, (fName, filedata))
                    tk.messagebox.showinfo("Success", "File successfully uploaded")
                    fileName.delete(0, 'end')
                    connectiondb.commit()
                    connectiondb.close()
                except:
                    tk.messagebox.showerror("File Error", "Error: Filename already in use")

        def remove_file():
            if fileName.get() == "":
                tk.messagebox.showerror("File Name Error", "Please enter the name of the file you wish to remove")
            else:
                try:
                    fName = fileName.get()
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                    cursordb = connectiondb.cursor()
                    cursordb.execute("DELETE FROM securesoftwarelifecycle WHERE name = %s", [(fName)])
                    tk.messagebox.showinfo("Success", "File successfully deleted")
                    fileName.delete(0, 'end')
                    connectiondb.commit()
                    connectiondb.close()
                except:
                    tk.messagebox.showerror("File Error", "Error: File Not Found")

        def update_file():
            if fileName.get() == "":
                tk.messagebox.showerror("File Name Error", "Please enter the name of the file you wish to update")
            else:
                try:
                    file = filedialog.askopenfilename(initialdir="/home/pi", title="Select file to add", filetypes=(("All Files", "*.*"), ("Exe files", "*.exe"), ("PDF files", "*.pdf")))
                    fName = fileName.get()
                    with open(file, "rb") as f:
                        filedata = f.read()
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                    cursordb = connectiondb.cursor()
                    cursordb.execute("UPDATE securesoftwarelifecycle SET filedata = %s WHERE name = %s", [(filedata), (fName)])
                    tk.messagebox.showinfo("Success", "File successfully updated")
                    fileName.delete(0, 'end')
                    connectiondb.commit()
                    connectiondb.close()
                except:
                    tk.messagebox.showerror("File Error", "Error: File Not Found")

        def run_file(fName):
                connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                cursordb = connectiondb.cursor()
                cursordb.execute("SELECT filedata FROM securesoftwarelifecycle WHERE name = %s", [(fName)])
                r = cursordb.fetchall()
                for i in r:
                    data = i[0]
                decodedData = data.decode("utf8")
                exec(decodedData)
                connectiondb.commit()
                connectiondb.close()

        def check_files():
            connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
            cursordb = connectiondb.cursor()
            cursordb.execute("SELECT name FROM securesoftwarelifecycle")
            r = cursordb.fetchall()
            connectiondb.commit()
            connectiondb.close()
            startX = 25
            startY = 275
            j = 0
            for i in r:
                button = Button(self, text=i, width=15)
                button.configure(bg="#83B4B3", activebackground="#CFF6FF", command=lambda: run_file(i[0]))
                button.place(x=startX, y=startY)
                startX = startX+200
                j = j+1
                if j == 6:
                    startY = startY+100
                    startX = 25
                    j = 0

        self.configure(bg="#B6F1FF")

        scrWidth = self.winfo_screenwidth()
        scrHeight = self.winfo_screenheight()
        check_files()

        logo_small(self, parent, root)

        # Configure logout button
        logoutBut = Button(self, text="Logout", width=15, command=lambda: root.show_frame(loginPage))
        logoutBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        logoutBut.place(x=scrWidth-185, y=25)

        # Configure help button
        helpBut = Button(self, text="Help", width=15, command=lambda: root.show_frame(staffHelpPage))
        helpBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        helpBut.place(x=scrWidth-360, y=25)

        # Configure account options button
        staffAccOpBut = Button(self, text="Account Options", width=15)
        staffAccOpBut.configure(bg="#83B4B3", activebackground="#CFF6FF", command=lambda: root.show_frame(staffAccountOpPage))
        staffAccOpBut.place(x=scrWidth-535, y=25)

        # Configure button to go back to staff landing page
        backBut = Button(self, text="Back", width=15, command=lambda: [self.lower(), check_files()])
        backBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        backBut.place(x=scrWidth-185, y=scrHeight-150)

        # Configure update button
        updateBut = Button(self, text="Update", width=15, command=lambda: [update_file(), check_files()])
        updateBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        updateBut.place(x=scrWidth-360, y=scrHeight-150)

        # Configure remove button
        removeBut = Button(self, text="Remove", width=15, command=lambda: [remove_file(), check_files()])
        removeBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        removeBut.place(x=scrWidth-535, y=scrHeight-150)

        # Configure add button
        addBut = Button(self, text="Add", width=15, command=lambda: [add_file(), check_files()])
        addBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        addBut.place(x=scrWidth-710, y=scrHeight-150)

        # Configure name entry
        fileName = Entry(self, bg="#FFFFFF", width=37)
        fileName.place(x=scrWidth-1040, y=scrHeight-150, height=31)

        file_label(self, parent, root)

class staffNetworkSecurity(tk.Frame):

    def __init__(self, parent, root):
        super().__init__(parent)

        def add_file():
            if fileName.get() == "":
                tk.messagebox.showerror("File Name Error", "Please enter the name you have chosen for the file you are uploading")
            else:
                try:
                    file = filedialog.askopenfilename(initialdir="/home/pi", title="Select file to add", filetypes=(("All Files", "*.*"), ("Python files", "*.py")))
                    fName = fileName.get()
                    with open(file, "rb") as f:
                        filedata = f.read()
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                    cursordb = connectiondb.cursor()
                    sql = "INSERT INTO networksecurity(name, filedata) VALUES(%s, %s)"
                    cursordb.execute(sql, (fName, filedata))
                    tk.messagebox.showinfo("Success", "File successfully uploaded")
                    fileName.delete(0, 'end')
                    connectiondb.commit()
                    connectiondb.close()
                except:
                    tk.messagebox.showerror("File Error", "Error: Filename already in use")

        def remove_file():
            if fileName.get() == "":
                tk.messagebox.showerror("File Name Error", "Please enter the name of the file you wish to remove")
            else:
                try:
                    fName = fileName.get()
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                    cursordb = connectiondb.cursor()
                    cursordb.execute("DELETE FROM networksecurity WHERE name = %s", [(fName)])
                    tk.messagebox.showinfo("Success", "File successfully deleted")
                    fileName.delete(0, 'end')
                    connectiondb.commit()
                    connectiondb.close()
                except:
                    tk.messagebox.showerror("File Error", "Error: File Not Found")

        def update_file():
            if fileName.get() == "":
                tk.messagebox.showerror("File Name Error", "Please enter the name of the file you wish to update")
            else:
                try:
                    file = filedialog.askopenfilename(initialdir="/home/pi", title="Select file to add", filetypes=(("All Files", "*.*"), ("Exe files", "*.exe"), ("PDF files", "*.pdf")))
                    fName = fileName.get()
                    with open(file, "rb") as f:
                        filedata = f.read()
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                    cursordb = connectiondb.cursor()
                    cursordb.execute("UPDATE networksecurity SET filedata = %s WHERE name = %s", [(filedata), (fName)])
                    tk.messagebox.showinfo("Success", "File successfully updated")
                    fileName.delete(0, 'end')
                    connectiondb.commit()
                    connectiondb.close()
                except:
                    tk.messagebox.showerror("File Error", "Error: File Not Found")

        def run_file(fName):
                connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                cursordb = connectiondb.cursor()
                cursordb.execute("SELECT filedata FROM networksecurity WHERE name = %s", [(fName)])
                r = cursordb.fetchall()
                for i in r:
                    data = i[0]
                decodedData = data.decode("utf8")
                exec(decodedData)
                connectiondb.commit()
                connectiondb.close()

        def check_files():
            connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
            cursordb = connectiondb.cursor()
            cursordb.execute("SELECT name FROM networksecurity")
            r = cursordb.fetchall()
            connectiondb.commit()
            connectiondb.close()
            startX = 25
            startY = 275
            j = 0
            for i in r:
                button = Button(self, text=i, width=15)
                button.configure(bg="#83B4B3", activebackground="#CFF6FF", command=lambda: run_file(i[0]))
                button.place(x=startX, y=startY)
                startX = startX+200
                j = j+1
                if j == 6:
                    startY = startY+100
                    startX = 25
                    j = 0

        self.configure(bg="#B6F1FF")

        scrWidth = self.winfo_screenwidth()
        scrHeight = self.winfo_screenheight()
        check_files()

        logo_small(self, parent, root)

        # Configure logout button
        logoutBut = Button(self, text="Logout", width=15, command=lambda: root.show_frame(loginPage))
        logoutBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        logoutBut.place(x=scrWidth-185, y=25)

        # Configure help button
        helpBut = Button(self, text="Help", width=15, command=lambda: root.show_frame(staffHelpPage))
        helpBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        helpBut.place(x=scrWidth-360, y=25)

        # Configure account options button
        staffAccOpBut = Button(self, text="Account Options", width=15)
        staffAccOpBut.configure(bg="#83B4B3", activebackground="#CFF6FF", command=lambda: root.show_frame(staffAccountOpPage))
        staffAccOpBut.place(x=scrWidth-535, y=25)

        # Configure button to go back to staff landing page
        backBut = Button(self, text="Back", width=15, command=lambda: [self.lower(), check_files()])
        backBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        backBut.place(x=scrWidth-185, y=scrHeight-150)

        # Configure update button
        updateBut = Button(self, text="Update", width=15, command=lambda: [update_file(), check_files()])
        updateBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        updateBut.place(x=scrWidth-360, y=scrHeight-150)

        # Configure remove button
        removeBut = Button(self, text="Remove", width=15, command=lambda: [remove_file(), check_files()])
        removeBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        removeBut.place(x=scrWidth-535, y=scrHeight-150)

        # Configure add button
        addBut = Button(self, text="Add", width=15, command=lambda: [add_file(), check_files()])
        addBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        addBut.place(x=scrWidth-710, y=scrHeight-150)

        # Configure name entry
        fileName = Entry(self, bg="#FFFFFF", width=37)
        fileName.place(x=scrWidth-1040, y=scrHeight-150, height=31)

        file_label(self, parent, root)

class staffHardwareSecurity(tk.Frame):

    def __init__(self, parent, root):
        super().__init__(parent)

        def add_file():
            if fileName.get() == "":
                tk.messagebox.showerror("File Name Error", "Please enter the name you have chosen for the file you are uploading")
            else:
                try:
                    file = filedialog.askopenfilename(initialdir="/home/pi", title="Select file to add", filetypes=(("All Files", "*.*"), ("Python files", "*.py")))
                    fName = fileName.get()
                    with open(file, "rb") as f:
                        filedata = f.read()
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                    cursordb = connectiondb.cursor()
                    sql = "INSERT INTO hardwaresecurity(name, filedata) VALUES(%s, %s)"
                    cursordb.execute(sql, (fName, filedata))
                    tk.messagebox.showinfo("Success", "File successfully uploaded")
                    fileName.delete(0, 'end')
                    connectiondb.commit()
                    connectiondb.close()
                except:
                    tk.messagebox.showerror("File Error", "Error: Filename already in use")

        def remove_file():
            if fileName.get() == "":
                tk.messagebox.showerror("File Name Error", "Please enter the name of the file you wish to remove")
            else:
                try:
                    fName = fileName.get()
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                    cursordb = connectiondb.cursor()
                    cursordb.execute("DELETE FROM hardwaresecurity WHERE name = %s", [(fName)])
                    tk.messagebox.showinfo("Success", "File successfully deleted")
                    fileName.delete(0, 'end')
                    connectiondb.commit()
                    connectiondb.close()
                except:
                    tk.messagebox.showerror("File Error", "Error: File Not Found")

        def update_file():
            if fileName.get() == "":
                tk.messagebox.showerror("File Name Error", "Please enter the name of the file you wish to update")
            else:
                try:
                    file = filedialog.askopenfilename(initialdir="/home/pi", title="Select file to add", filetypes=(("All Files", "*.*"), ("Exe files", "*.exe"), ("PDF files", "*.pdf")))
                    fName = fileName.get()
                    with open(file, "rb") as f:
                        filedata = f.read()
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                    cursordb = connectiondb.cursor()
                    cursordb.execute("UPDATE hardwaresecurity SET filedata = %s WHERE name = %s", [(filedata), (fName)])
                    tk.messagebox.showinfo("Success", "File successfully updated")
                    fileName.delete(0, 'end')
                    connectiondb.commit()
                    connectiondb.close()
                except:
                    tk.messagebox.showerror("File Error", "Error: File Not Found")

        def run_file(fName):
                connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                cursordb = connectiondb.cursor()
                cursordb.execute("SELECT filedata FROM hardwaresecurity WHERE name = %s", [(fName)])
                r = cursordb.fetchall()
                for i in r:
                    data = i[0]
                decodedData = data.decode("utf8")
                exec(decodedData)
                connectiondb.commit()
                connectiondb.close()

        def check_files():
            connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
            cursordb = connectiondb.cursor()
            cursordb.execute("SELECT name FROM hardwaresecurity")
            r = cursordb.fetchall()
            connectiondb.commit()
            connectiondb.close()
            startX = 25
            startY = 275
            j = 0
            for i in r:
                button = Button(self, text=i, width=15)
                button.configure(bg="#83B4B3", activebackground="#CFF6FF", command=lambda: run_file(i[0]))
                button.place(x=startX, y=startY)
                startX = startX+200
                j = j+1
                if j == 6:
                    startY = startY+100
                    startX = 25
                    j = 0

        self.configure(bg="#B6F1FF")

        scrWidth = self.winfo_screenwidth()
        scrHeight = self.winfo_screenheight()
        check_files()

        logo_small(self, parent, root)

        # Configure logout button
        logoutBut = Button(self, text="Logout", width=15, command=lambda: root.show_frame(loginPage))
        logoutBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        logoutBut.place(x=scrWidth-185, y=25)

        # Configure help button
        helpBut = Button(self, text="Help", width=15, command=lambda: root.show_frame(staffHelpPage))
        helpBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        helpBut.place(x=scrWidth-360, y=25)

        # Configure account options button
        staffAccOpBut = Button(self, text="Account Options", width=15)
        staffAccOpBut.configure(bg="#83B4B3", activebackground="#CFF6FF", command=lambda: root.show_frame(staffAccountOpPage))
        staffAccOpBut.place(x=scrWidth-535, y=25)

        # Configure button to go back to staff landing page
        backBut = Button(self, text="Back", width=15, command=lambda: [self.lower(), check_files()])
        backBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        backBut.place(x=scrWidth-185, y=scrHeight-150)

        # Configure update button
        updateBut = Button(self, text="Update", width=15, command=lambda: [update_file(), check_files()])
        updateBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        updateBut.place(x=scrWidth-360, y=scrHeight-150)

        # Configure remove button
        removeBut = Button(self, text="Remove", width=15, command=lambda: [remove_file(), check_files()])
        removeBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        removeBut.place(x=scrWidth-535, y=scrHeight-150)

        # Configure add button
        addBut = Button(self, text="Add", width=15, command=lambda: [add_file(), check_files()])
        addBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        addBut.place(x=scrWidth-710, y=scrHeight-150)

        # Configure name entry
        fileName = Entry(self, bg="#FFFFFF", width=37)
        fileName.place(x=scrWidth-1040, y=scrHeight-150, height=31)

        file_label(self, parent, root)

class staffCyberPhysicalSystems(tk.Frame):

    def __init__(self, parent, root):
        super().__init__(parent)

        def add_file():
            if fileName.get() == "":
                tk.messagebox.showerror("File Name Error", "Please enter the name you have chosen for the file you are uploading")
            else:
                try:
                    file = filedialog.askopenfilename(initialdir="/home/pi", title="Select file to add", filetypes=(("All Files", "*.*"), ("Python files", "*.py")))
                    fName = fileName.get()
                    with open(file, "rb") as f:
                        filedata = f.read()
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                    cursordb = connectiondb.cursor()
                    sql = "INSERT INTO cyberphysicalsystems(name, filedata) VALUES(%s, %s)"
                    cursordb.execute(sql, (fName, filedata))
                    tk.messagebox.showinfo("Success", "File successfully uploaded")
                    fileName.delete(0, 'end')
                    connectiondb.commit()
                    connectiondb.close()
                except:
                    tk.messagebox.showerror("File Error", "Error: Filename already in use")

        def remove_file():
            if fileName.get() == "":
                tk.messagebox.showerror("File Name Error", "Please enter the name of the file you wish to remove")
            else:
                try:
                    fName = fileName.get()
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                    cursordb = connectiondb.cursor()
                    cursordb.execute("DELETE FROM cyberphysicalsystems WHERE name = %s", [(fName)])
                    tk.messagebox.showinfo("Success", "File successfully deleted")
                    fileName.delete(0, 'end')
                    connectiondb.commit()
                    connectiondb.close()
                except:
                    tk.messagebox.showerror("File Error", "Error: File Not Found")

        def update_file():
            if fileName.get() == "":
                tk.messagebox.showerror("File Name Error", "Please enter the name of the file you wish to update")
            else:
                try:
                    file = filedialog.askopenfilename(initialdir="/home/pi", title="Select file to add", filetypes=(("All Files", "*.*"), ("Exe files", "*.exe"), ("PDF files", "*.pdf")))
                    fName = fileName.get()
                    with open(file, "rb") as f:
                        filedata = f.read()
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                    cursordb = connectiondb.cursor()
                    cursordb.execute("UPDATE cyberphysicalsystems SET filedata = %s WHERE name = %s", [(filedata), (fName)])
                    tk.messagebox.showinfo("Success", "File successfully updated")
                    fileName.delete(0, 'end')
                    connectiondb.commit()
                    connectiondb.close()
                except:
                    tk.messagebox.showerror("File Error", "Error: File Not Found")

        def run_file(fName):
                connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                cursordb = connectiondb.cursor()
                cursordb.execute("SELECT filedata FROM cyberphysicalsystems WHERE name = %s", [(fName)])
                r = cursordb.fetchall()
                for i in r:
                    data = i[0]
                decodedData = data.decode("utf8")
                exec(decodedData)
                connectiondb.commit()
                connectiondb.close()

        def check_files():
            connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
            cursordb = connectiondb.cursor()
            cursordb.execute("SELECT name FROM cyberphysicalsystems")
            r = cursordb.fetchall()
            connectiondb.commit()
            connectiondb.close()
            startX = 25
            startY = 275
            j = 0
            for i in r:
                button = Button(self, text=i, width=15)
                button.configure(bg="#83B4B3", activebackground="#CFF6FF", command=lambda: run_file(i[0]))
                button.place(x=startX, y=startY)
                startX = startX+200
                j = j+1
                if j == 6:
                    startY = startY+100
                    startX = 25
                    j = 0

        self.configure(bg="#B6F1FF")

        scrWidth = self.winfo_screenwidth()
        scrHeight = self.winfo_screenheight()
        check_files()

        logo_small(self, parent, root)

        # Configure logout button
        logoutBut = Button(self, text="Logout", width=15, command=lambda: root.show_frame(loginPage))
        logoutBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        logoutBut.place(x=scrWidth-185, y=25)

        # Configure help button
        helpBut = Button(self, text="Help", width=15, command=lambda: root.show_frame(staffHelpPage))
        helpBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        helpBut.place(x=scrWidth-360, y=25)

        # Configure account options button
        staffAccOpBut = Button(self, text="Account Options", width=15)
        staffAccOpBut.configure(bg="#83B4B3", activebackground="#CFF6FF", command=lambda: root.show_frame(staffAccountOpPage))
        staffAccOpBut.place(x=scrWidth-535, y=25)

        # Configure button to go back to staff landing page
        backBut = Button(self, text="Back", width=15, command=lambda: [self.lower(), check_files()])
        backBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        backBut.place(x=scrWidth-185, y=scrHeight-150)

        # Configure update button
        updateBut = Button(self, text="Update", width=15, command=lambda: [update_file(), check_files()])
        updateBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        updateBut.place(x=scrWidth-360, y=scrHeight-150)

        # Configure remove button
        removeBut = Button(self, text="Remove", width=15, command=lambda: [remove_file(), check_files()])
        removeBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        removeBut.place(x=scrWidth-535, y=scrHeight-150)

        # Configure add button
        addBut = Button(self, text="Add", width=15, command=lambda: [add_file(), check_files()])
        addBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        addBut.place(x=scrWidth-710, y=scrHeight-150)

        # Configure name entry
        fileName = Entry(self, bg="#FFFFFF", width=37)
        fileName.place(x=scrWidth-1040, y=scrHeight-150, height=31)

        file_label(self, parent, root)

class staffPLandTS(tk.Frame):

    def __init__(self, parent, root):
        super().__init__(parent)

        def add_file():
            if fileName.get() == "":
                tk.messagebox.showerror("File Name Error", "Please enter the name you have chosen for the file you are uploading")
            else:
                try:
                    file = filedialog.askopenfilename(initialdir="/home/pi", title="Select file to add", filetypes=(("All Files", "*.*"), ("Python files", "*.py")))
                    fName = fileName.get()
                    with open(file, "rb") as f:
                        filedata = f.read()
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                    cursordb = connectiondb.cursor()
                    sql = "INSERT INTO physicallayerandtelecommunicationssecurity(name, filedata) VALUES(%s, %s)"
                    cursordb.execute(sql, (fName, filedata))
                    tk.messagebox.showinfo("Success", "File successfully uploaded")
                    fileName.delete(0, 'end')
                    connectiondb.commit()
                    connectiondb.close()
                except:
                    tk.messagebox.showerror("File Error", "Error: Filename already in use")

        def remove_file():
            if fileName.get() == "":
                tk.messagebox.showerror("File Name Error", "Please enter the name of the file you wish to remove")
            else:
                try:
                    fName = fileName.get()
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                    cursordb = connectiondb.cursor()
                    cursordb.execute("DELETE FROM physicallayerandtelecommunicationssecurity WHERE name = %s", [(fName)])
                    tk.messagebox.showinfo("Success", "File successfully deleted")
                    fileName.delete(0, 'end')
                    connectiondb.commit()
                    connectiondb.close()
                except:
                    tk.messagebox.showerror("File Error", "Error: File Not Found")

        def update_file():
            if fileName.get() == "":
                tk.messagebox.showerror("File Name Error", "Please enter the name of the file you wish to update")
            else:
                try:
                    file = filedialog.askopenfilename(initialdir="/home/pi", title="Select file to add", filetypes=(("All Files", "*.*"), ("Exe files", "*.exe"), ("PDF files", "*.pdf")))
                    fName = fileName.get()
                    with open(file, "rb") as f:
                        filedata = f.read()
                    connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                    cursordb = connectiondb.cursor()
                    cursordb.execute("UPDATE physicallayerandtelecommunicationssecurity SET filedata = %s WHERE name = %s", [(filedata), (fName)])
                    tk.messagebox.showinfo("Success", "File successfully updated")
                    fileName.delete(0, 'end')
                    connectiondb.commit()
                    connectiondb.close()
                except:
                    tk.messagebox.showerror("File Error", "Error: File Not Found")

        def run_file(fName):
                connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
                cursordb = connectiondb.cursor()
                cursordb.execute("SELECT filedata FROM physicallayerandtelecommunicationssecurity WHERE name = %s", [(fName)])
                r = cursordb.fetchall()
                for i in r:
                    data = i[0]
                decodedData = data.decode("utf8")
                exec(decodedData)
                connectiondb.commit()
                connectiondb.close()

        def check_files():
            connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
            cursordb = connectiondb.cursor()
            cursordb.execute("SELECT name FROM physicallayerandtelecommunicationssecurity")
            r = cursordb.fetchall()
            connectiondb.commit()
            connectiondb.close()
            startX = 25
            startY = 275
            j = 0
            for i in r:
                button = Button(self, text=i, width=15)
                button.configure(bg="#83B4B3", activebackground="#CFF6FF", command=lambda: run_file(i[0]))
                button.place(x=startX, y=startY)
                startX = startX+200
                j = j+1
                if j == 6:
                    startY = startY+100
                    startX = 25
                    j = 0

        self.configure(bg="#B6F1FF")

        scrWidth = self.winfo_screenwidth()
        scrHeight = self.winfo_screenheight()
        check_files()

        logo_small(self, parent, root)

        # Configure logout button
        logoutBut = Button(self, text="Logout", width=15, command=lambda: root.show_frame(loginPage))
        logoutBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        logoutBut.place(x=scrWidth-185, y=25)

        # Configure help button
        helpBut = Button(self, text="Help", width=15, command=lambda: root.show_frame(staffHelpPage))
        helpBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        helpBut.place(x=scrWidth-360, y=25)

        # Configure account options button
        staffAccOpBut = Button(self, text="Account Options", width=15)
        staffAccOpBut.configure(bg="#83B4B3", activebackground="#CFF6FF", command=lambda: root.show_frame(staffAccountOpPage))
        staffAccOpBut.place(x=scrWidth-535, y=25)

        # Configure button to go back to staff landing page
        backBut = Button(self, text="Back", width=15, command=lambda: [self.lower(), check_files()])
        backBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        backBut.place(x=scrWidth-185, y=scrHeight-150)

        # Configure update button
        updateBut = Button(self, text="Update", width=15, command=lambda: [update_file(), check_files()])
        updateBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        updateBut.place(x=scrWidth-360, y=scrHeight-150)

        # Configure remove button
        removeBut = Button(self, text="Remove", width=15, command=lambda: [remove_file(), check_files()])
        removeBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        removeBut.place(x=scrWidth-535, y=scrHeight-150)

        # Configure add button
        addBut = Button(self, text="Add", width=15, command=lambda: [add_file(), check_files()])
        addBut.configure(bg="#83B4B3", activebackground="#CFF6FF")
        addBut.place(x=scrWidth-710, y=scrHeight-150)

        # Configure name entry
        fileName = Entry(self, bg="#FFFFFF", width=37)
        fileName.place(x=scrWidth-1040, y=scrHeight-150, height=31)

        file_label(self, parent, root)

class studentRMandG(tk.Frame):                      # Class for the student risk management and governance page
    def __init__(self, parent, root):
        super().__init__(parent)

        def run_file(fName):                        # Func to run the selected file from the page
            connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
            cursordb = connectiondb.cursor()
            cursordb.execute("SELECT filedata FROM riskmanagementandgovernance WHERE name=%s", [(fName)])
            r = cursordb.fetchall()
            for i in r:
                data = i[0]
            decodedData = data.decode("utf8")
            exec(decodedData)
            connectiondb.commit()
            connectiondb.close()

        def check_files():                          # Func to display and allow running of files from the database
            connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
            cursordb = connectiondb.cursor()
            cursordb.execute("SELECT name FROM riskmanagementandgovernance")
            r = cursordb.fetchall()
            connectiondb.commit()
            connectiondb.close()
            startX = 25
            startY = 275
            j = 0
            for i in r:
                button = Button(self, text=i, width=15)
                button.configure(bg="#83B4B3", activebackground="#097392", command=lambda: (run_file(i[0])))
                button.place(x=startX, y=startY)
                startX = startX+200
                j = j+1
                if j == 6:
                    startY = startY+100
                    startX = 25
                    j = 0

        self.configure(bg="#CFF6FF")

        scrWidth = self.winfo_screenwidth()
        scrHeight = self.winfo_screenheight()
        check_files()

        logo_small(self, parent, root)

        logoutBut = Button(self, text="Logout", width=15, command=lambda: root.show_frame(loginPage))
        logoutBut.configure(bg="#83B4B3", activebackground="#097392")
        logoutBut.place(x=scrWidth-185, y=25)

        helpBut = Button(self, text="Help", width=15, command=lambda: root.show_frame(studentHelpPage))
        helpBut.configure(bg="#83B4B3", activebackground="#097392")
        helpBut.place(x=scrWidth-360, y=25)

        studentAccOpBut = Button(self, text="Account Options", width=15)
        studentAccOpBut.configure(bg="#83B4B3", activebackground="#097392", command=lambda: root.show_frame(studentAccountOpPage))
        studentAccOpBut.place(x=scrWidth-535, y=25)

        backBut = Button(self, text="Back", width=15, command=lambda: [self.lower(), check_files()])
        backBut.configure(bg="#83B4B3", activebackground="#097392")
        backBut.place(x=scrWidth-185, y=scrHeight-150)

class studentLawandReg(tk.Frame):

    def __init__(self, parent, root):
        super().__init__(parent)

        def run_file(fName):
            connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
            cursordb = connectiondb.cursor()
            cursordb.execute("SELECT filedata FROM lawandregulation WHERE name=%s", [(fName)])
            r = cursordb.fetchall()
            for i in r:
                data = i[0]
            decodedData = data.decode("utf8")
            exec(decodedData)
            connectiondb.commit()
            connectiondb.close()

        def check_files():
            connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
            cursordb = connectiondb.cursor()
            cursordb.execute("SELECT name FROM lawandregulation")
            r = cursordb.fetchall()
            connectiondb.commit()
            connectiondb.close()
            startX = 25
            startY = 275
            j = 0
            for i in r:
                button = Button(self, text=i, width=15)
                button.configure(bg="#83B4B3", activebackground="#097392", command=lambda: (run_file(i[0])))
                button.place(x=startX, y=startY)
                startX = startX+200
                j = j+1
                if j == 6:
                    startY = startY+100
                    startX = 25
                    j = 0

        self.configure(bg="#CFF6FF")

        scrWidth = self.winfo_screenwidth()
        scrHeight = self.winfo_screenheight()
        check_files()

        logo_small(self, parent, root)

        # Configure logout button
        logoutBut = Button(self, text="Logout", width=15, command=lambda: root.show_frame(loginPage))
        logoutBut.configure(bg="#83B4B3", activebackground="#097392")
        logoutBut.place(x=scrWidth-185, y=25)

        # Configure help button
        helpBut = Button(self, text="Help", width=15, command=lambda: root.show_frame(studentHelpPage))
        helpBut.configure(bg="#83B4B3", activebackground="#097392")
        helpBut.place(x=scrWidth-360, y=25)

        # Configure account options button
        studentAccOpBut = Button(self, text="Account Options", width=15)
        studentAccOpBut.configure(bg="#83B4B3", activebackground="#097392", command=lambda: root.show_frame(studentAccountOpPage))
        studentAccOpBut.place(x=scrWidth-535, y=25)

        # Configure button to go back to student landing page
        backBut = Button(self, text="Back", width=15, command=lambda: [self.lower(), check_files()])
        backBut.configure(bg="#83B4B3", activebackground="#097392")
        backBut.place(x=scrWidth-185, y=scrHeight-150)

class studentHumanFactors(tk.Frame):

    def __init__(self, parent, root):
        super().__init__(parent)

        def run_file(fName):
            connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
            cursordb = connectiondb.cursor()
            cursordb.execute("SELECT filedata FROM humanfactors WHERE name=%s", [(fName)])
            r = cursordb.fetchall()
            for i in r:
                data = i[0]
            decodedData = data.decode("utf8")
            exec(decodedData)
            connectiondb.commit()
            connectiondb.close()

        def check_files():
            connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
            cursordb = connectiondb.cursor()
            cursordb.execute("SELECT name FROM humanfactors")
            r = cursordb.fetchall()
            connectiondb.commit()
            connectiondb.close()
            startX = 25
            startY = 275
            j = 0
            for i in r:
                button = Button(self, text=i, width=15)
                button.configure(bg="#83B4B3", activebackground="#097392", command=lambda: (run_file(i[0])))
                button.place(x=startX, y=startY)
                startX = startX+200
                j = j+1
                if j == 6:
                    startY = startY+100
                    startX = 25
                    j = 0

        self.configure(bg="#CFF6FF")

        scrWidth = self.winfo_screenwidth()
        scrHeight = self.winfo_screenheight()
        check_files()

        logo_small(self, parent, root)

        # Configure logout button
        logoutBut = Button(self, text="Logout", width=15, command=lambda: root.show_frame(loginPage))
        logoutBut.configure(bg="#83B4B3", activebackground="#097392")
        logoutBut.place(x=scrWidth-185, y=25)

        # Configure help button
        helpBut = Button(self, text="Help", width=15, command=lambda: root.show_frame(studentHelpPage))
        helpBut.configure(bg="#83B4B3", activebackground="#097392")
        helpBut.place(x=scrWidth-360, y=25)

        # Configure account options button
        studentAccOpBut = Button(self, text="Account Options", width=15)
        studentAccOpBut.configure(bg="#83B4B3", activebackground="#097392", command=lambda: root.show_frame(studentAccountOpPage))
        studentAccOpBut.place(x=scrWidth-535, y=25)

        # Configure button to go back to student landing page
        backBut = Button(self, text="Back", width=15, command=lambda: [self.lower(), check_files()])
        backBut.configure(bg="#83B4B3", activebackground="#097392")
        backBut.place(x=scrWidth-185, y=scrHeight-150)

class studentPandOR(tk.Frame):

    def __init__(self, parent, root):
        super().__init__(parent)

        def run_file(fName):
            connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
            cursordb = connectiondb.cursor()
            cursordb.execute("SELECT filedata FROM privacyandonlinerights WHERE name=%s", [(fName)])
            r = cursordb.fetchall()
            for i in r:
                data = i[0]
            decodedData = data.decode("utf8")
            exec(decodedData)
            connectiondb.commit()
            connectiondb.close()

        def check_files():
            connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
            cursordb = connectiondb.cursor()
            cursordb.execute("SELECT name FROM privacyandonlinerights")
            r = cursordb.fetchall()
            connectiondb.commit()
            connectiondb.close()
            startX = 25
            startY = 275
            j = 0
            for i in r:
                button = Button(self, text=i, width=15)
                button.configure(bg="#83B4B3", activebackground="#097392", command=lambda: (run_file(i[0])))
                button.place(x=startX, y=startY)
                startX = startX+200
                j = j+1
                if j == 6:
                    startY = startY+100
                    startX = 25
                    j = 0

        self.configure(bg="#CFF6FF")

        scrWidth = self.winfo_screenwidth()
        scrHeight = self.winfo_screenheight()
        check_files()

        logo_small(self, parent, root)

        # Configure logout button
        logoutBut = Button(self, text="Logout", width=15, command=lambda: root.show_frame(loginPage))
        logoutBut.configure(bg="#83B4B3", activebackground="#097392")
        logoutBut.place(x=scrWidth-185, y=25)

        # Configure help button
        helpBut = Button(self, text="Help", width=15, command=lambda: root.show_frame(studentHelpPage))
        helpBut.configure(bg="#83B4B3", activebackground="#097392")
        helpBut.place(x=scrWidth-360, y=25)

        # Configure account options button
        studentAccOpBut = Button(self, text="Account Options", width=15)
        studentAccOpBut.configure(bg="#83B4B3", activebackground="#097392", command=lambda: root.show_frame(studentAccountOpPage))
        studentAccOpBut.place(x=scrWidth-535, y=25)

        # Configure button to go back to student landing page
        backBut = Button(self, text="Back", width=15, command=lambda: [self.lower(), check_files()])
        backBut.configure(bg="#83B4B3", activebackground="#097392")
        backBut.place(x=scrWidth-185, y=scrHeight-150)

class studentMandAT(tk.Frame):

    def __init__(self, parent, root):
        super().__init__(parent)

        def run_file(fName):
            connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
            cursordb = connectiondb.cursor()
            cursordb.execute("SELECT filedata FROM malwareandattacktechnologies WHERE name=%s", [(fName)])
            r = cursordb.fetchall()
            for i in r:
                data = i[0]
            decodedData = data.decode("utf8")
            exec(decodedData)
            connectiondb.commit()
            connectiondb.close()

        def check_files():
            connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
            cursordb = connectiondb.cursor()
            cursordb.execute("SELECT name FROM malwareandattacktechnologies")
            r = cursordb.fetchall()
            connectiondb.commit()
            connectiondb.close()
            startX = 25
            startY = 275
            j = 0
            for i in r:
                button = Button(self, text=i, width=15)
                button.configure(bg="#83B4B3", activebackground="#097392", command=lambda: (run_file(i[0])))
                button.place(x=startX, y=startY)
                startX = startX+200
                j = j+1
                if j == 6:
                    startY = startY+100
                    startX = 25
                    j = 0

        self.configure(bg="#CFF6FF")

        scrWidth = self.winfo_screenwidth()
        scrHeight = self.winfo_screenheight()
        check_files()

        logo_small(self, parent, root)

        # Configure logout button
        logoutBut = Button(self, text="Logout", width=15, command=lambda: root.show_frame(loginPage))
        logoutBut.configure(bg="#83B4B3", activebackground="#097392")
        logoutBut.place(x=scrWidth-185, y=25)

        # Configure help button
        helpBut = Button(self, text="Help", width=15, command=lambda: root.show_frame(studentHelpPage))
        helpBut.configure(bg="#83B4B3", activebackground="#097392")
        helpBut.place(x=scrWidth-360, y=25)

        # Configure account options button
        studentAccOpBut = Button(self, text="Account Options", width=15)
        studentAccOpBut.configure(bg="#83B4B3", activebackground="#097392", command=lambda: root.show_frame(studentAccountOpPage))
        studentAccOpBut.place(x=scrWidth-535, y=25)

        # Configure button to go back to student landing page
        backBut = Button(self, text="Back", width=15, command=lambda: [self.lower(), check_files()])
        backBut.configure(bg="#83B4B3", activebackground="#097392")
        backBut.place(x=scrWidth-185, y=scrHeight-150)

class studentAdversarialBehaviours(tk.Frame):

    def __init__(self, parent, root):
        super().__init__(parent)

        def run_file(fName):
            connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
            cursordb = connectiondb.cursor()
            cursordb.execute("SELECT filedata FROM adversarialbehaviours WHERE name=%s", [(fName)])
            r = cursordb.fetchall()
            for i in r:
                data = i[0]
            decodedData = data.decode("utf8")
            exec(decodedData)
            connectiondb.commit()
            connectiondb.close()

        def check_files():
            connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
            cursordb = connectiondb.cursor()
            cursordb.execute("SELECT name FROM adversarialbehaviours")
            r = cursordb.fetchall()
            connectiondb.commit()
            connectiondb.close()
            startX = 25
            startY = 275
            j = 0
            for i in r:
                button = Button(self, text=i, width=15)
                button.configure(bg="#83B4B3", activebackground="#097392", command=lambda: (run_file(i[0])))
                button.place(x=startX, y=startY)
                startX = startX+200
                j = j+1
                if j == 6:
                    startY = startY+100
                    startX = 25
                    j = 0

        self.configure(bg="#CFF6FF")

        scrWidth = self.winfo_screenwidth()
        scrHeight = self.winfo_screenheight()
        check_files()

        logo_small(self, parent, root)

        # Configure logout button
        logoutBut = Button(self, text="Logout", width=15, command=lambda: root.show_frame(loginPage))
        logoutBut.configure(bg="#83B4B3", activebackground="#097392")
        logoutBut.place(x=scrWidth-185, y=25)

        # Configure help button
        helpBut = Button(self, text="Help", width=15, command=lambda: root.show_frame(studentHelpPage))
        helpBut.configure(bg="#83B4B3", activebackground="#097392")
        helpBut.place(x=scrWidth-360, y=25)

        # Configure account options button
        studentAccOpBut = Button(self, text="Account Options", width=15)
        studentAccOpBut.configure(bg="#83B4B3", activebackground="#097392", command=lambda: root.show_frame(studentAccountOpPage))
        studentAccOpBut.place(x=scrWidth-535, y=25)

        # Configure button to go back to student landing page
        backBut = Button(self, text="Back", width=15, command=lambda: [self.lower(), check_files()])
        backBut.configure(bg="#83B4B3", activebackground="#097392")
        backBut.place(x=scrWidth-185, y=scrHeight-150)

class studentSOandIM(tk.Frame):

    def __init__(self, parent, root):
        super().__init__(parent)

        def run_file(fName):
            connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
            cursordb = connectiondb.cursor()
            cursordb.execute("SELECT filedata FROM securityoperationsandincidentmanagement WHERE name=%s", [(fName)])
            r = cursordb.fetchall()
            for i in r:
                data = i[0]
            decodedData = data.decode("utf8")
            exec(decodedData)
            connectiondb.commit()
            connectiondb.close()

        def check_files():
            connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
            cursordb = connectiondb.cursor()
            cursordb.execute("SELECT name FROM securityoperationsandincidentmanagement")
            r = cursordb.fetchall()
            connectiondb.commit()
            connectiondb.close()
            startX = 25
            startY = 275
            j = 0
            for i in r:
                button = Button(self, text=i, width=15)
                button.configure(bg="#83B4B3", activebackground="#097392", command=lambda: (run_file(i[0])))
                button.place(x=startX, y=startY)
                startX = startX+200
                j = j+1
                if j == 6:
                    startY = startY+100
                    startX = 25
                    j = 0

        self.configure(bg="#CFF6FF")

        scrWidth = self.winfo_screenwidth()
        scrHeight = self.winfo_screenheight()
        check_files()

        logo_small(self, parent, root)

        # Configure logout button
        logoutBut = Button(self, text="Logout", width=15, command=lambda: root.show_frame(loginPage))
        logoutBut.configure(bg="#83B4B3", activebackground="#097392")
        logoutBut.place(x=scrWidth-185, y=25)

        # Configure help button
        helpBut = Button(self, text="Help", width=15, command=lambda: root.show_frame(studentHelpPage))
        helpBut.configure(bg="#83B4B3", activebackground="#097392")
        helpBut.place(x=scrWidth-360, y=25)

        # Configure account options button
        studentAccOpBut = Button(self, text="Account Options", width=15)
        studentAccOpBut.configure(bg="#83B4B3", activebackground="#097392", command=lambda: root.show_frame(studentAccountOpPage))
        studentAccOpBut.place(x=scrWidth-535, y=25)

        # Configure button to go back to student landing page
        backBut = Button(self, text="Back", width=15, command=lambda: [self.lower(), check_files()])
        backBut.configure(bg="#83B4B3", activebackground="#097392")
        backBut.place(x=scrWidth-185, y=scrHeight-150)

class studentForensics(tk.Frame):

    def __init__(self, parent, root):
        super().__init__(parent)

        def run_file(fName):
            connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
            cursordb = connectiondb.cursor()
            cursordb.execute("SELECT filedata FROM forensics WHERE name=%s", [(fName)])
            r = cursordb.fetchall()
            for i in r:
                data = i[0]
            decodedData = data.decode("utf8")
            exec(decodedData)
            connectiondb.commit()
            connectiondb.close()

        def check_files():
            connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
            cursordb = connectiondb.cursor()
            cursordb.execute("SELECT name FROM forensics")
            r = cursordb.fetchall()
            connectiondb.commit()
            connectiondb.close()
            startX = 25
            startY = 275
            j = 0
            for i in r:
                button = Button(self, text=i, width=15)
                button.configure(bg="#83B4B3", activebackground="#097392", command=lambda: (run_file(i[0])))
                button.place(x=startX, y=startY)
                startX = startX+200
                j = j+1
                if j == 6:
                    startY = startY+100
                    startX = 25
                    j = 0

        self.configure(bg="#CFF6FF")

        scrWidth = self.winfo_screenwidth()
        scrHeight = self.winfo_screenheight()
        check_files()

        logo_small(self, parent, root)

        # Configure logout button
        logoutBut = Button(self, text="Logout", width=15, command=lambda: root.show_frame(loginPage))
        logoutBut.configure(bg="#83B4B3", activebackground="#097392")
        logoutBut.place(x=scrWidth-185, y=25)

        # Configure help button
        helpBut = Button(self, text="Help", width=15, command=lambda: root.show_frame(studentHelpPage))
        helpBut.configure(bg="#83B4B3", activebackground="#097392")
        helpBut.place(x=scrWidth-360, y=25)

        # Configure account options button
        studentAccOpBut = Button(self, text="Account Options", width=15)
        studentAccOpBut.configure(bg="#83B4B3", activebackground="#097392", command=lambda: root.show_frame(studentAccountOpPage))
        studentAccOpBut.place(x=scrWidth-535, y=25)

        # Configure button to go back to student landing page
        backBut = Button(self, text="Back", width=15, command=lambda: [self.lower(), check_files()])
        backBut.configure(bg="#83B4B3", activebackground="#097392")
        backBut.place(x=scrWidth-185, y=scrHeight-150)

class studentCryprography(tk.Frame):

    def __init__(self, parent, root):
        super().__init__(parent)

        def run_file(fName):
            connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
            cursordb = connectiondb.cursor()
            cursordb.execute("SELECT filedata FROM cryptography WHERE name=%s", [(fName)])
            r = cursordb.fetchall()
            for i in r:
                data = i[0]
            decodedData = data.decode("utf8")
            exec(decodedData)
            connectiondb.commit()
            connectiondb.close()

        def check_files():
            connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
            cursordb = connectiondb.cursor()
            cursordb.execute("SELECT name FROM cryptography")
            r = cursordb.fetchall()
            connectiondb.commit()
            connectiondb.close()
            startX = 25
            startY = 275
            j = 0
            for i in r:
                button = Button(self, text=i, width=15)
                button.configure(bg="#83B4B3", activebackground="#097392", command=lambda: (run_file(i[0])))
                button.place(x=startX, y=startY)
                startX = startX+200
                j = j+1
                if j == 6:
                    startY = startY+100
                    startX = 25
                    j = 0

        self.configure(bg="#CFF6FF")

        scrWidth = self.winfo_screenwidth()
        scrHeight = self.winfo_screenheight()
        check_files()

        logo_small(self, parent, root)

        # Configure logout button
        logoutBut = Button(self, text="Logout", width=15, command=lambda: root.show_frame(loginPage))
        logoutBut.configure(bg="#83B4B3", activebackground="#097392")
        logoutBut.place(x=scrWidth-185, y=25)

        # Configure help button
        helpBut = Button(self, text="Help", width=15, command=lambda: root.show_frame(studentHelpPage))
        helpBut.configure(bg="#83B4B3", activebackground="#097392")
        helpBut.place(x=scrWidth-360, y=25)

        # Configure account options button
        studentAccOpBut = Button(self, text="Account Options", width=15)
        studentAccOpBut.configure(bg="#83B4B3", activebackground="#097392", command=lambda: root.show_frame(studentAccountOpPage))
        studentAccOpBut.place(x=scrWidth-535, y=25)

        # Configure button to go back to student landing page
        backBut = Button(self, text="Back", width=15, command=lambda: [self.lower(), check_files()])
        backBut.configure(bg="#83B4B3", activebackground="#097392")
        backBut.place(x=scrWidth-185, y=scrHeight-150)

class studentOSandVS(tk.Frame):

    def __init__(self, parent, root):
        super().__init__(parent)

        def run_file(fName):
            connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
            cursordb = connectiondb.cursor()
            cursordb.execute("SELECT filedata FROM operatingsystemsandvirtualisationsecurity WHERE name=%s", [(fName)])
            r = cursordb.fetchall()
            for i in r:
                data = i[0]
            decodedData = data.decode("utf8")
            exec(decodedData)
            connectiondb.commit()
            connectiondb.close()

        def check_files():
            connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
            cursordb = connectiondb.cursor()
            cursordb.execute("SELECT name FROM operatingsystemsandvirtualisationsecurity")
            r = cursordb.fetchall()
            connectiondb.commit()
            connectiondb.close()
            startX = 25
            startY = 275
            j = 0
            for i in r:
                button = Button(self, text=i, width=15)
                button.configure(bg="#83B4B3", activebackground="#097392", command=lambda: (run_file(i[0])))
                button.place(x=startX, y=startY)
                startX = startX+200
                j = j+1
                if j == 6:
                    startY = startY+100
                    startX = 25
                    j = 0

        self.configure(bg="#CFF6FF")

        scrWidth = self.winfo_screenwidth()
        scrHeight = self.winfo_screenheight()
        check_files()

        logo_small(self, parent, root)

        # Configure logout button
        logoutBut = Button(self, text="Logout", width=15, command=lambda: root.show_frame(loginPage))
        logoutBut.configure(bg="#83B4B3", activebackground="#097392")
        logoutBut.place(x=scrWidth-185, y=25)

        # Configure help button
        helpBut = Button(self, text="Help", width=15, command=lambda: root.show_frame(studentHelpPage))
        helpBut.configure(bg="#83B4B3", activebackground="#097392")
        helpBut.place(x=scrWidth-360, y=25)

        # Configure account options button
        studentAccOpBut = Button(self, text="Account Options", width=15)
        studentAccOpBut.configure(bg="#83B4B3", activebackground="#097392", command=lambda: root.show_frame(studentAccountOpPage))
        studentAccOpBut.place(x=scrWidth-535, y=25)

        # Configure button to go back to student landing page
        backBut = Button(self, text="Back", width=15, command=lambda: [self.lower(), check_files()])
        backBut.configure(bg="#83B4B3", activebackground="#097392")
        backBut.place(x=scrWidth-185, y=scrHeight-150)

class studentDSS(tk.Frame):

    def __init__(self, parent, root):
        super().__init__(parent)

        def run_file(fName):
            connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
            cursordb = connectiondb.cursor()
            cursordb.execute("SELECT filedata FROM distributedsystemssecurity WHERE name=%s", [(fName)])
            r = cursordb.fetchall()
            for i in r:
                data = i[0]
            decodedData = data.decode("utf8")
            exec(decodedData)
            connectiondb.commit()
            connectiondb.close()

        def check_files():
            connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
            cursordb = connectiondb.cursor()
            cursordb.execute("SELECT name FROM distributedsystemssecurity")
            r = cursordb.fetchall()
            connectiondb.commit()
            connectiondb.close()
            startX = 25
            startY = 275
            j = 0
            for i in r:
                button = Button(self, text=i, width=15)
                button.configure(bg="#83B4B3", activebackground="#097392", command=lambda: (run_file(i[0])))
                button.place(x=startX, y=startY)
                startX = startX+200
                j = j+1
                if j == 6:
                    startY = startY+100
                    startX = 25
                    j = 0

        self.configure(bg="#CFF6FF")

        scrWidth = self.winfo_screenwidth()
        scrHeight = self.winfo_screenheight()
        check_files()

        logo_small(self, parent, root)

        # Configure logout button
        logoutBut = Button(self, text="Logout", width=15, command=lambda: root.show_frame(loginPage))
        logoutBut.configure(bg="#83B4B3", activebackground="#097392")
        logoutBut.place(x=scrWidth-185, y=25)

        # Configure help button
        helpBut = Button(self, text="Help", width=15, command=lambda: root.show_frame(studentHelpPage))
        helpBut.configure(bg="#83B4B3", activebackground="#097392")
        helpBut.place(x=scrWidth-360, y=25)

        # Configure account options button
        studentAccOpBut = Button(self, text="Account Options", width=15)
        studentAccOpBut.configure(bg="#83B4B3", activebackground="#097392", command=lambda: root.show_frame(studentAccountOpPage))
        studentAccOpBut.place(x=scrWidth-535, y=25)

        # Configure button to go back to student landing page
        backBut = Button(self, text="Back", width=15, command=lambda: [self.lower(), check_files()])
        backBut.configure(bg="#83B4B3", activebackground="#097392")
        backBut.place(x=scrWidth-185, y=scrHeight-150)

class studentAAandA(tk.Frame):

    def __init__(self, parent, root):
        super().__init__(parent)

        def run_file(fName):
            connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
            cursordb = connectiondb.cursor()
            cursordb.execute("SELECT filedata FROM authenticationauthorisationandaccountability WHERE name=%s", [(fName)])
            r = cursordb.fetchall()
            for i in r:
                data = i[0]
            decodedData = data.decode("utf8")
            exec(decodedData)
            connectiondb.commit()
            connectiondb.close()

        def check_files():
            connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
            cursordb = connectiondb.cursor()
            cursordb.execute("SELECT name FROM authenticationauthorisationandaccountability")
            r = cursordb.fetchall()
            connectiondb.commit()
            connectiondb.close()
            startX = 25
            startY = 275
            j = 0
            for i in r:
                button = Button(self, text=i, width=15)
                button.configure(bg="#83B4B3", activebackground="#097392", command=lambda: (run_file(i[0])))
                button.place(x=startX, y=startY)
                startX = startX+200
                j = j+1
                if j == 6:
                    startY = startY+100
                    startX = 25
                    j = 0

        self.configure(bg="#CFF6FF")

        scrWidth = self.winfo_screenwidth()
        scrHeight = self.winfo_screenheight()
        check_files()

        logo_small(self, parent, root)

        # Configure logout button
        logoutBut = Button(self, text="Logout", width=15, command=lambda: root.show_frame(loginPage))
        logoutBut.configure(bg="#83B4B3", activebackground="#097392")
        logoutBut.place(x=scrWidth-185, y=25)

        # Configure help button
        helpBut = Button(self, text="Help", width=15, command=lambda: root.show_frame(studentHelpPage))
        helpBut.configure(bg="#83B4B3", activebackground="#097392")
        helpBut.place(x=scrWidth-360, y=25)

        # Configure account options button
        studentAccOpBut = Button(self, text="Account Options", width=15)
        studentAccOpBut.configure(bg="#83B4B3", activebackground="#097392", command=lambda: root.show_frame(studentAccountOpPage))
        studentAccOpBut.place(x=scrWidth-535, y=25)

        # Configure button to go back to student landing page
        backBut = Button(self, text="Back", width=15, command=lambda: [self.lower(), check_files()])
        backBut.configure(bg="#83B4B3", activebackground="#097392")
        backBut.place(x=scrWidth-185, y=scrHeight-150)

class studentSoftwareSecurity(tk.Frame):

    def __init__(self, parent, root):
        super().__init__(parent)

        def run_file(fName):
            connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
            cursordb = connectiondb.cursor()
            cursordb.execute("SELECT filedata FROM softwaresecurity WHERE name=%s", [(fName)])
            r = cursordb.fetchall()
            for i in r:
                data = i[0]
            decodedData = data.decode("utf8")
            exec(decodedData)
            connectiondb.commit()
            connectiondb.close()

        def check_files():
            connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
            cursordb = connectiondb.cursor()
            cursordb.execute("SELECT name FROM softwaresecurity")
            r = cursordb.fetchall()
            connectiondb.commit()
            connectiondb.close()
            startX = 25
            startY = 275
            j = 0
            for i in r:
                button = Button(self, text=i, width=15)
                button.configure(bg="#83B4B3", activebackground="#097392", command=lambda: (run_file(i[0])))
                button.place(x=startX, y=startY)
                startX = startX+200
                j = j+1
                if j == 6:
                    startY = startY+100
                    startX = 25
                    j = 0

        self.configure(bg="#CFF6FF")

        scrWidth = self.winfo_screenwidth()
        scrHeight = self.winfo_screenheight()
        check_files()

        logo_small(self, parent, root)

        # Configure logout button
        logoutBut = Button(self, text="Logout", width=15, command=lambda: root.show_frame(loginPage))
        logoutBut.configure(bg="#83B4B3", activebackground="#097392")
        logoutBut.place(x=scrWidth-185, y=25)

        # Configure help button
        helpBut = Button(self, text="Help", width=15, command=lambda: root.show_frame(studentHelpPage))
        helpBut.configure(bg="#83B4B3", activebackground="#097392")
        helpBut.place(x=scrWidth-360, y=25)

        # Configure account options button
        studentAccOpBut = Button(self, text="Account Options", width=15)
        studentAccOpBut.configure(bg="#83B4B3", activebackground="#097392", command=lambda: root.show_frame(studentAccountOpPage))
        studentAccOpBut.place(x=scrWidth-535, y=25)

        # Configure button to go back to student landing page
        backBut = Button(self, text="Back", width=15, command=lambda: [self.lower(), check_files()])
        backBut.configure(bg="#83B4B3", activebackground="#097392")
        backBut.place(x=scrWidth-185, y=scrHeight-150)

class studentWandMS(tk.Frame):

    def __init__(self, parent, root):
        super().__init__(parent)

        def run_file(fName):
            connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
            cursordb = connectiondb.cursor()
            cursordb.execute("SELECT filedata FROM webandmobilesecurity WHERE name=%s", [(fName)])
            r = cursordb.fetchall()
            for i in r:
                data = i[0]
            decodedData = data.decode("utf8")
            exec(decodedData)
            connectiondb.commit()
            connectiondb.close()

        def check_files():
            connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
            cursordb = connectiondb.cursor()
            cursordb.execute("SELECT name FROM webandmobilesecurity")
            r = cursordb.fetchall()
            connectiondb.commit()
            connectiondb.close()
            startX = 25
            startY = 275
            j = 0
            for i in r:
                button = Button(self, text=i, width=15)
                button.configure(bg="#83B4B3", activebackground="#097392", command=lambda: (run_file(i[0])))
                button.place(x=startX, y=startY)
                startX = startX+200
                j = j+1
                if j == 6:
                    startY = startY+100
                    startX = 25
                    j = 0

        self.configure(bg="#CFF6FF")

        scrWidth = self.winfo_screenwidth()
        scrHeight = self.winfo_screenheight()
        check_files()

        logo_small(self, parent, root)

        # Configure logout button
        logoutBut = Button(self, text="Logout", width=15, command=lambda: root.show_frame(loginPage))
        logoutBut.configure(bg="#83B4B3", activebackground="#097392")
        logoutBut.place(x=scrWidth-185, y=25)

        # Configure help button
        helpBut = Button(self, text="Help", width=15, command=lambda: root.show_frame(studentHelpPage))
        helpBut.configure(bg="#83B4B3", activebackground="#097392")
        helpBut.place(x=scrWidth-360, y=25)

        # Configure account options button
        studentAccOpBut = Button(self, text="Account Options", width=15)
        studentAccOpBut.configure(bg="#83B4B3", activebackground="#097392", command=lambda: root.show_frame(studentAccountOpPage))
        studentAccOpBut.place(x=scrWidth-535, y=25)

        # Configure button to go back to student landing page
        backBut = Button(self, text="Back", width=15, command=lambda: [self.lower(), check_files()])
        backBut.configure(bg="#83B4B3", activebackground="#097392")
        backBut.place(x=scrWidth-185, y=scrHeight-150)

class studentSSL(tk.Frame):

    def __init__(self, parent, root):
        super().__init__(parent)

        def run_file(fName):
            connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
            cursordb = connectiondb.cursor()
            cursordb.execute("SELECT filedata FROM securesoftwarelifecycle WHERE name=%s", [(fName)])
            r = cursordb.fetchall()
            for i in r:
                data = i[0]
            decodedData = data.decode("utf8")
            exec(decodedData)
            connectiondb.commit()
            connectiondb.close()

        def check_files():
            connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
            cursordb = connectiondb.cursor()
            cursordb.execute("SELECT name FROM securesoftwarelifecycle")
            r = cursordb.fetchall()
            connectiondb.commit()
            connectiondb.close()
            startX = 25
            startY = 275
            j = 0
            for i in r:
                button = Button(self, text=i, width=15)
                button.configure(bg="#83B4B3", activebackground="#097392", command=lambda: (run_file(i[0])))
                button.place(x=startX, y=startY)
                startX = startX+200
                j = j+1
                if j == 6:
                    startY = startY+100
                    startX = 25
                    j = 0

        self.configure(bg="#CFF6FF")

        scrWidth = self.winfo_screenwidth()
        scrHeight = self.winfo_screenheight()
        check_files()

        logo_small(self, parent, root)

        # Configure logout button
        logoutBut = Button(self, text="Logout", width=15, command=lambda: root.show_frame(loginPage))
        logoutBut.configure(bg="#83B4B3", activebackground="#097392")
        logoutBut.place(x=scrWidth-185, y=25)

        # Configure help button
        helpBut = Button(self, text="Help", width=15, command=lambda: root.show_frame(studentHelpPage))
        helpBut.configure(bg="#83B4B3", activebackground="#097392")
        helpBut.place(x=scrWidth-360, y=25)

        # Configure account options button
        studentAccOpBut = Button(self, text="Account Options", width=15)
        studentAccOpBut.configure(bg="#83B4B3", activebackground="#097392", command=lambda: root.show_frame(studentAccountOpPage))
        studentAccOpBut.place(x=scrWidth-535, y=25)

        # Configure button to go back to student landing page
        backBut = Button(self, text="Back", width=15, command=lambda: [self.lower(), check_files()])
        backBut.configure(bg="#83B4B3", activebackground="#097392")
        backBut.place(x=scrWidth-185, y=scrHeight-150)

class studentNetworkSecurity(tk.Frame):

    def __init__(self, parent, root):
        super().__init__(parent)

        def run_file(fName):
            connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
            cursordb = connectiondb.cursor()
            cursordb.execute("SELECT filedata FROM networksecurity WHERE name=%s", [(fName)])
            r = cursordb.fetchall()
            for i in r:
                data = i[0]
            decodedData = data.decode("utf8")
            exec(decodedData)
            connectiondb.commit()
            connectiondb.close()

        def check_files():
            connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
            cursordb = connectiondb.cursor()
            cursordb.execute("SELECT name FROM networksecurity")
            r = cursordb.fetchall()
            connectiondb.commit()
            connectiondb.close()
            startX = 25
            startY = 275
            j = 0
            for i in r:
                button = Button(self, text=i, width=15)
                button.configure(bg="#83B4B3", activebackground="#097392", command=lambda: (run_file(i[0])))
                button.place(x=startX, y=startY)
                startX = startX+200
                j = j+1
                if j == 6:
                    startY = startY+100
                    startX = 25
                    j = 0

        self.configure(bg="#CFF6FF")

        scrWidth = self.winfo_screenwidth()
        scrHeight = self.winfo_screenheight()
        check_files()

        logo_small(self, parent, root)

        # Configure logout button
        logoutBut = Button(self, text="Logout", width=15, command=lambda: root.show_frame(loginPage))
        logoutBut.configure(bg="#83B4B3", activebackground="#097392")
        logoutBut.place(x=scrWidth-185, y=25)

        # Configure help button
        helpBut = Button(self, text="Help", width=15, command=lambda: root.show_frame(studentHelpPage))
        helpBut.configure(bg="#83B4B3", activebackground="#097392")
        helpBut.place(x=scrWidth-360, y=25)

        # Configure account options button
        studentAccOpBut = Button(self, text="Account Options", width=15)
        studentAccOpBut.configure(bg="#83B4B3", activebackground="#097392", command=lambda: root.show_frame(studentAccountOpPage))
        studentAccOpBut.place(x=scrWidth-535, y=25)

        # Configure button to go back to student landing page
        backBut = Button(self, text="Back", width=15, command=lambda: [self.lower(), check_files()])
        backBut.configure(bg="#83B4B3", activebackground="#097392")
        backBut.place(x=scrWidth-185, y=scrHeight-150)

class studentHardwareSecurity(tk.Frame):

    def __init__(self, parent, root):
        super().__init__(parent)

        def run_file(fName):
            connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
            cursordb = connectiondb.cursor()
            cursordb.execute("SELECT filedata FROM hardwaresecurity WHERE name=%s", [(fName)])
            r = cursordb.fetchall()
            for i in r:
                data = i[0]
            decodedData = data.decode("utf8")
            exec(decodedData)
            connectiondb.commit()
            connectiondb.close()

        def check_files():
            connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
            cursordb = connectiondb.cursor()
            cursordb.execute("SELECT name FROM hardwaresecurity")
            r = cursordb.fetchall()
            connectiondb.commit()
            connectiondb.close()
            startX = 25
            startY = 275
            j = 0
            for i in r:
                button = Button(self, text=i, width=15)
                button.configure(bg="#83B4B3", activebackground="#097392", command=lambda: (run_file(i[0])))
                button.place(x=startX, y=startY)
                startX = startX+200
                j = j+1
                if j == 6:
                    startY = startY+100
                    startX = 25
                    j = 0

        self.configure(bg="#CFF6FF")

        scrWidth = self.winfo_screenwidth()
        scrHeight = self.winfo_screenheight()
        check_files()

        logo_small(self, parent, root)

        # Configure logout button
        logoutBut = Button(self, text="Logout", width=15, command=lambda: root.show_frame(loginPage))
        logoutBut.configure(bg="#83B4B3", activebackground="#097392")
        logoutBut.place(x=scrWidth-185, y=25)

        # Configure help button
        helpBut = Button(self, text="Help", width=15, command=lambda: root.show_frame(studentHelpPage))
        helpBut.configure(bg="#83B4B3", activebackground="#097392")
        helpBut.place(x=scrWidth-360, y=25)

        # Configure account options button
        studentAccOpBut = Button(self, text="Account Options", width=15)
        studentAccOpBut.configure(bg="#83B4B3", activebackground="#097392", command=lambda: root.show_frame(studentAccountOpPage))
        studentAccOpBut.place(x=scrWidth-535, y=25)

        # Configure button to go back to student landing page
        backBut = Button(self, text="Back", width=15, command=lambda: [self.lower(), check_files()])
        backBut.configure(bg="#83B4B3", activebackground="#097392")
        backBut.place(x=scrWidth-185, y=scrHeight-150)

class studentCyberPhysicalSystems(tk.Frame):

    def __init__(self, parent, root):
        super().__init__(parent)

        def run_file(fName):
            connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
            cursordb = connectiondb.cursor()
            cursordb.execute("SELECT filedata FROM cyberphysicalsystems WHERE name=%s", [(fName)])
            r = cursordb.fetchall()
            for i in r:
                data = i[0]
            decodedData = data.decode("utf8")
            exec(decodedData)
            connectiondb.commit()
            connectiondb.close()

        def check_files():
            connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
            cursordb = connectiondb.cursor()
            cursordb.execute("SELECT name FROM cyberphysicalsystems")
            r = cursordb.fetchall()
            connectiondb.commit()
            connectiondb.close()
            startX = 25
            startY = 275
            j = 0
            for i in r:
                button = Button(self, text=i, width=15)
                button.configure(bg="#83B4B3", activebackground="#097392", command=lambda: (run_file(i[0])))
                button.place(x=startX, y=startY)
                startX = startX+200
                j = j+1
                if j == 6:
                    startY = startY+100
                    startX = 25
                    j = 0

        self.configure(bg="#CFF6FF")

        scrWidth = self.winfo_screenwidth()
        scrHeight = self.winfo_screenheight()
        check_files()

        logo_small(self, parent, root)

        # Configure logout button
        logoutBut = Button(self, text="Logout", width=15, command=lambda: root.show_frame(loginPage))
        logoutBut.configure(bg="#83B4B3", activebackground="#097392")
        logoutBut.place(x=scrWidth-185, y=25)

        # Configure help button
        helpBut = Button(self, text="Help", width=15, command=lambda: root.show_frame(studentHelpPage))
        helpBut.configure(bg="#83B4B3", activebackground="#097392")
        helpBut.place(x=scrWidth-360, y=25)

        # Configure account options button
        studentAccOpBut = Button(self, text="Account Options", width=15)
        studentAccOpBut.configure(bg="#83B4B3", activebackground="#097392", command=lambda: root.show_frame(studentAccountOpPage))
        studentAccOpBut.place(x=scrWidth-535, y=25)

        # Configure button to go back to student landing page
        backBut = Button(self, text="Back", width=15, command=lambda: [self.lower(), check_files()])
        backBut.configure(bg="#83B4B3", activebackground="#097392")
        backBut.place(x=scrWidth-185, y=scrHeight-150)

class studentPLandTS(tk.Frame):

    def __init__(self, parent, root):
        super().__init__(parent)

        def run_file(fName):
            connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
            cursordb = connectiondb.cursor()
            cursordb.execute("SELECT filedata FROM physicallayerandtelecommunicationssecurity WHERE name=%s", [(fName)])
            r = cursordb.fetchall()
            for i in r:
                data = i[0]
            decodedData = data.decode("utf8")
            exec(decodedData)
            connectiondb.commit()
            connectiondb.close()

        def check_files():
            connectiondb = mysql.connector.connect(user='admin', password='admin', host='localhost', database='cypilearning')
            cursordb = connectiondb.cursor()
            cursordb.execute("SELECT name FROM physicallayerandtelecommunicationssecurity")
            r = cursordb.fetchall()
            connectiondb.commit()
            connectiondb.close()
            startX = 25
            startY = 275
            j = 0
            for i in r:
                button = Button(self, text=i, width=15)
                button.configure(bg="#83B4B3", activebackground="#097392", command=lambda: (run_file(i[0])))
                button.place(x=startX, y=startY)
                startX = startX+200
                j = j+1
                if j == 6:
                    startY = startY+100
                    startX = 25
                    j = 0

        self.configure(bg="#CFF6FF")

        scrWidth = self.winfo_screenwidth()
        scrHeight = self.winfo_screenheight()
        check_files()

        logo_small(self, parent, root)

        # Configure logout button
        logoutBut = Button(self, text="Logout", width=15, command=lambda: root.show_frame(loginPage))
        logoutBut.configure(bg="#83B4B3", activebackground="#097392")
        logoutBut.place(x=scrWidth-185, y=25)

        # Configure help button
        helpBut = Button(self, text="Help", width=15, command=lambda: root.show_frame(studentHelpPage))
        helpBut.configure(bg="#83B4B3", activebackground="#097392")
        helpBut.place(x=scrWidth-360, y=25)

        # Configure account options button
        studentAccOpBut = Button(self, text="Account Options", width=15)
        studentAccOpBut.configure(bg="#83B4B3", activebackground="#097392", command=lambda: root.show_frame(studentAccountOpPage))
        studentAccOpBut.place(x=scrWidth-535, y=25)

        # Configure button to go back to student landing page
        backBut = Button(self, text="Back", width=15, command=lambda: [self.lower(), check_files()])
        backBut.configure(bg="#83B4B3", activebackground="#097392")
        backBut.place(x=scrWidth-185, y=scrHeight-150)

if __name__ == "__main__":                      # Code to run the application class
    app = Application()
    app.mainloop()