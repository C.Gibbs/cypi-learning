=====================================================
CyPi-Learning Application
=====================================================

Description
=====================================================

This program is designed to introduce cyber-security and its principles to 
students, as part of a self-contained outreach project on a RaspberryPi 3B+.

Features
=====================================================

*Provides a completely self-contained set of learning tasks
*Provides student and admin accounts to limit access to functions based on user level

Authors
=====================================================

Chloe E Gibbs, 2021

Requirements
=====================================================

*Python 3
*Tkinter
*RaspberryPi 3B+
*16GB MicroSD Card

Usage
=====================================================

To start the application, run:
	
	python3 CyPi_Learning/cypi_learning.py

To find out your Pi's IP address for the database, go to the terminal and type:

	ifconfig

General Notes
=====================================================

