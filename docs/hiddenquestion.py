import tkinter as tk
from tkinter import *
from tkinter import Entry, Label, Button, messagebox, filedialog


class Application(tk.Tk):

    def __init__(self):
        super().__init__()

        self.title("CyPi-Learning Activity")
        scrWidth = self.winfo_screenwidth()
        scrHeight = self.winfo_screenheight()
        ctr = (scrWidth/2)-150
        self.geometry("%dx%d" % (scrWidth, scrHeight))
        self.resizable(width=False, height=False)

        container = tk.Frame(self)
        container.pack(side="top", fill="both", expand=True)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        container.configure(background="#DAEDFD")

        """START CODE FOR ACTIVITY HERE"""
        def correct_ans():
            tk.messagebox.showinfo("Correct", "Congrats, that was the correct answer!", master=container)

        def wrong_ans():
            tk.messagebox.showinfo("Wrong", "Sorry, that was the wrong answer!", master=container)

        instructLabel = Label(container, text="To answer this question you will need to explore the Rapsberry Pi.")
        instructLabel.configure(bg="#DAEDFD")
        instructLabel.place(x=ctr-50, y=300)

        questionLabel = Label(container, text="Where on this Pi, have we hidden a .txt file named 'hidden'?")
        questionLabel.configure(bg="#DAEDFD")
        questionLabel.place(x=ctr-40, y= 350)

        ansOneBut = Button(container, text="/home/pi/.vnc")
        ansOneBut.configure(bg="#83B4B3", activebackground="#097392", command=lambda: [correct_ans(), self.destroy()])
        ansOneBut.place(x=(scrWidth/2)-55, y=400)

        ansTwoBut = Button(container, text="/home/pi/Music")
        ansTwoBut.configure(bg="#83B4B3", activebackground="#097392", command=lambda: wrong_ans())
        ansTwoBut.place(x=(scrWidth/2)-55, y=450)

        ansThreeBut = Button(container, text="/home/pi/mu_code")
        ansThreeBut.configure(bg="#83B4B3", activebackground="#097392", command=lambda: wrong_ans())
        ansThreeBut.place(x=(scrWidth/2)-65, y=500)

        """END CODE FOR ACTIVITY HERE"""

        # Configure button to close activity
        closeBut = Button(container, text="Close activity", width=15, command=lambda: [self.lower(), self.destroy()])
        closeBut.configure(bg="#83B4B3", activebackground="#097392")
        closeBut.place(x=scrWidth-185, y=scrHeight-150)

if __name__ == "__main__":
    app = Application()
    app.mainloop()