import tkinter as tk
from tkinter import *
from tkinter import Entry, Label, Button, messagebox, filedialog


class Application(tk.Tk):

    def __init__(self):
        super().__init__()

        self.title("CyPi-Learning Activity")
        scrWidth = self.winfo_screenwidth()
        scrHeight = self.winfo_screenheight()
        self.geometry("%dx%d" % (scrWidth, scrHeight))
        self.resizable(width=False, height=False)

        container = tk.Frame(self)
        container.pack(side="top", fill="both", expand=True)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        container.configure(background="#DAEDFD")
        
        """START CODE FOR ACTIVITY HERE"""
        
        """END CODE FOR ACTIVITY HERE"""

        # Configure button to close activity
        closeBut = Button(container, text="Close activity", width=15, command=lambda: [self.lower(), self.destroy()])
        closeBut.configure(bg="#83B4B3", activebackground="#097392")
        closeBut.place(x=scrWidth-185, y=scrHeight-150)

if __name__ == "__main__":
    app = Application()
    app.mainloop()